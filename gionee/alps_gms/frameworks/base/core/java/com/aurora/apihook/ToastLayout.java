package com.aurora.apihook;

import java.lang.reflect.Member;

import android.content.Context;
import android.util.AttributeSet;
import android.util.Log;
import android.view.Gravity;
import android.widget.LinearLayout;


public  class ToastLayout extends LinearLayout{

	private int mX,mY;
	private int mGravity;
	public ToastLayout(Context context, AttributeSet attrs, int defStyle) {
		super(context, attrs, defStyle);
		// TODO Auto-generated constructor stub
	}

	public ToastLayout(Context context, AttributeSet attrs) {
		super(context, attrs);
		// TODO Auto-generated constructor stub
	}

	public ToastLayout(Context context) {
		super(context);
		// TODO Auto-generated constructor stub
	}
	
	public void setXY(int x,int y){
		this.mX = x;
		this.mY = y;
	}
	
	public void setGravity(int gravity,int xoffset,int yoffset){
		this.mGravity = gravity;
		mX = xoffset;
		if(mGravity == Gravity.BOTTOM && yoffset == 0){
			mY = 1;
		}else if(mGravity == Gravity.CENTER && yoffset == 0){
			mY = -10;
		}else{
			mY = -yoffset +1;
		}
	}
	
	@Override
	public void getLocationOnScreen(int[] location) {
		// TODO Auto-generated method stub
//		super.getLocationOnScreen(location);
		location[0] = mX;
		location[1] = mY;
	}
	
}
