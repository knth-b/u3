/*************************************************************************************
 * 
 * Description:
 * 	Defines ArcSoft APIs for night shot.
 *
 * Author : zhangwu
 * Email  : zhangwu@gionee.com
 * Date   : 2013-09-07
 *
 *************************************************************************************/

#define LOG_TAG "ArcSoftNightVideo"
#include <android/log.h>
#include <string.h>

#include "ArcSoftNightVideo.h"

#define MEM_SIZE	5 * 1024 * 1024 + (3120 * 4208 * 3 / 2) * 5

namespace android { 

ArcSoftNightVideo::ArcSoftNightVideo()
	: mMem(NULL)
	, mMemMgr(NULL)
	, mEnhancer(NULL)
{
	
}
	
int32 
ArcSoftNightVideo::init() 
{
	MRESULT res = 0; 
	
	memset(&mParam, 0, sizeof(mParam));

	pthread_mutex_init(&mMutex, NULL);

	return res;
}
void  
ArcSoftNightVideo::deinit()
{
	ANH_Uninit(&mEnhancer);

	if(mMemMgr != NULL) {
		MMemMgrDestroy(mMemMgr);
		mMemMgr = NULL;
	}

	if(mMem != NULL) {
		MMemFree(NULL, mMem);
		mMem = NULL;
	}

	pthread_mutex_destroy(&mMutex); 
}

int32 
ArcSoftNightVideo::processNightVideo(LPASVLOFFSCREEN param)
{
	MRESULT res = 0;

	//pthread_mutex_lock(&mMutex);

	ASVLOFFSCREEN dstImg;

	dstImg.i32Width = param->i32Width;
	dstImg.i32Height = param->i32Height;
	dstImg.u32PixelArrayFormat = ASVL_PAF_NV21;
	dstImg.pi32Pitch[0] = param->i32Width;
	dstImg.ppu8Plane[0] = param->ppu8Plane[0];
	dstImg.pi32Pitch[1] = param->i32Width;
	dstImg.ppu8Plane[1] = param->ppu8Plane[1];
	
	res = ANH_Enhance(mEnhancer, param, &dstImg, &mParam);
	if (res != MOK) {
		PRINTD("%s ANH_Enhance faile [%ld]", __func__, res);
	} 

	//pthread_mutex_unlock(&mMutex);

	return res;
}

int32 
ArcSoftNightVideo::enableNightVideo()
{
	MRESULT res = 0;

	if (mMem == NULL) {
		mMem = MMemAlloc(NULL, MEM_SIZE);
		mMemMgr = MMemMgrCreate(mMem, MEM_SIZE);

		res = ANH_Init(mMemMgr, &mEnhancer);
		if (res != 0) {
			PRINTE("Failed to initialize nightshot enginer [#%ld].", res);
			deinit();
		} else {
			res = ANH_GetDefaultParam(&mParam);
			PRINTD("%s lIntensity %d lDenoise %d", __func__, mParam.lIntensity, mParam.lDenoise);
			mParam.lIntensity = mParam.lIntensity - 30;
			mParam.lDenoise = 10;
			if (res != MOK) {
				PRINTD("%s ANH_GetDefaultParam failed", __func__);
			}
		}
	}

	return res;
}

int32 
ArcSoftNightVideo::disableNightVideo()
{
	MRESULT res = 0;

	deinit();

	return res;
}

};


