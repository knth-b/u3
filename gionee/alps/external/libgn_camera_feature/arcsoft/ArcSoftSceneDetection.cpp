/*************************************************************************************
 * 
 * Description:
 * 	Defines ArcSoft APIs for scene detection.
 *
 * Author : wutangzhi
 * Email  : wutz@gionee.com
 * Date   : 2013-09-07
 *
 *************************************************************************************/

#define LOG_TAG "ArcSoftSceneDetection"
#include <android/log.h>
#include <string.h>
 
#include "ArcSoftSceneDetection.h"

namespace android { 

#define SCENE_TYPE_SIZE	9
#define MEMSIZE 5 * 1024 * 1024

static ASD_SCENETYPE mAsdSceneTypes[] = {
									ASD_AUTO, 
									ASD_PORTRAIT, 
									ASD_BACKLIT, 
									ASD_PORTRAIT_NIGHT, 
									ASD_PORTRAIT_BACKLIT, 
									ASD_NIGHT, 
									ASD_GOURMET,
									ASD_TEXT,
									ASD_SPORT
									};
static GNCameraSceneDetectionType_t mGnCameraSceneTypes[] = {
									GN_CAMERA_SCENE_DETECTION_TYPE_AUTO,
									GN_CAMERA_SCENE_DETECTION_TYPE_PORTRAIT,
									GN_CAMERA_SCENE_DETECTION_TYPE_BACKLIT,
									GN_CAMERA_SCENE_DETECTION_TYPE_PORTRAIT_NIGHT,
									GN_CAMERA_SCENE_DETECTION_TYPE_PORTRAIT_BACKLIT,
									GN_CAMERA_SCENE_DETECTION_TYPE_NIGHT,
									GN_CAMERA_SCENE_DETECTION_TYPE_GOURMET,
									GN_CAMERA_SCENE_DETECTION_TYPE_TEXT,
									GN_CAMERA_SCENE_DETECTION_TYPE_SPORT
									};

ArcSoftSceneDetection::ArcSoftSceneDetection()
	: mASD(MNull)
	, mListener(NULL)
	, mMem(MNull)
	, mMemMgr(MNull)
	, mInitialized(false)
{
	memset(&mHWParam, 0, sizeof(mHWParam));

	pthread_mutex_init(&mMutex, NULL);
}

ArcSoftSceneDetection::~ArcSoftSceneDetection()
{
	pthread_mutex_destroy(&mMutex);
}

int32 
ArcSoftSceneDetection::init()
{
	MRESULT res = 0; 

	return res;
}

void  
ArcSoftSceneDetection::deinit()
{
	if (mInitialized) {
		ASD_UnInit(mASD);
	}

	if(mMemMgr != NULL) {
		MMemMgrDestroy(mMemMgr);
		mMemMgr = NULL;
	}

	if(mMem != NULL) {
		MMemFree(NULL, mMem);
		mMem = NULL;
	}

	mInitialized = false;
}

int32 
ArcSoftSceneDetection::setCameraListener(GNCameraFeatureListener* listener)
{
	MRESULT res = 0; 

	pthread_mutex_lock(&mMutex);
	mListener = listener;
	pthread_mutex_unlock(&mMutex);

	return res;
}

int32
ArcSoftSceneDetection::setOrientation(int orientation)
{
	MRESULT res = 0;

	pthread_mutex_lock(&mMutex);
	mOrientation = orientation;
	pthread_mutex_unlock(&mMutex);
	return res;
}

int32 
ArcSoftSceneDetection::processSceneDetection(LPASVLOFFSCREEN param)
{
	MRESULT res = 0;
	ASD_SCENETYPE sceneType = ASD_AUTO;

	if (!mInitialized) {
		return res;
	}

	res = ASD_SceneDetector(mASD, param, &mHWParam, &sceneType);
	if (res != 0) {
		PRINTD("%s Scene Detection failed [%ld]", __func__, res);
		return res;
	}

	PRINTD("%s sceneType = %d", __func__, sceneType);

	if (mListener != NULL) {
		mListener->notify(GN_CAMERA_MSG_TYPE_SCENE_DETECTION, getSceneDetetionType(sceneType), 0);
	}
	
	return res;
}

int32 
ArcSoftSceneDetection::enableSceneDetection()
{
	MRESULT res = 0; 

	if (!mInitialized) {
		mMem = (MByte *)MMemAlloc(MNull, MEMSIZE);
		if (mMem == MNull) {
			PRINTD("%s MMemAlloc failed", __func__);
			return -1;
		}
		
		mMemMgr = MMemMgrCreate(mMem, MEMSIZE);
		if (mMemMgr == MNull) {
			PRINTD("%s MMemMgrCreate failed", __func__);
			MMemFree(MNull,mMem);
			return -1;
		}

		res = ASD_Init(mMemMgr, &mASD);
		if (res != 0) {
			PRINTD("%s ASD_Init failed", __func__);
			
			if(MNull != mMem) {
				MMemFree(MNull,mMem);
			}
			MMemMgrDestroy(mMemMgr);

			return -1;
		}

		mHWParam.wFaces = 0;
		mHWParam.stFaces[0] = {0};

		MDWord dwModeCofig = ASD_PORTRAIT | ASD_PORTRAIT_NIGHT
							| ASD_PORTRAIT_BACKLIT | ASD_NIGHT 
							| ASD_BACKLIT | ASD_GOURMET | ASD_TEXT;
		
		mHWParam.wOrientation = 0;//this value should be set based on mOrientation; 
		mHWParam.m_modeProfile = dwModeCofig;

		mInitialized = true;
	}

	return res;
}

int32 
ArcSoftSceneDetection::disableSceneDetection()
{
	MRESULT res = 0; 

	deinit();
	
	return res;
}

int32
ArcSoftSceneDetection::getSceneDetetionType(int type)
{
	int res = 0;
	int i = 0;

	for (i = 0; i < SCENE_TYPE_SIZE; i ++) {
		if (type == (int)mAsdSceneTypes[i]) {
			break;
		}
	}

	if (i < SCENE_TYPE_SIZE) {
		res = (int)mGnCameraSceneTypes[i];
	}
	
 	return res;
}

};
