#ifndef TA3D_COMMON_H
#define TA3D_COMMON_H

#ifdef ANDROID
#  define TA3D_EXPORT __attribute__ ((visibility ("default")))
#elif defined(_WIN32)
#  define TA3D_EXPORT __declspec(dllexport)
#else
#  define TA3D_EXPORT
#endif

#endif  // TA3D_COMMON_H
