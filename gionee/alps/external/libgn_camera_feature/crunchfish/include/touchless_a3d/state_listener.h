#ifndef STATE_LISTENER_H
#define STATE_LISTENER_H

#include "common.h"
#include "engine_state.h"

namespace TouchlessA3D {

  /**
   * Abstract base class for state listeners implemented by the user of this library.
   *
   * Copyright © 2014 Crunchfish AB. All rights reserved. All information herein
   * is or may be trade secrets of Crunchfish AB.
   */
  class TA3D_EXPORT StateListener {
  public:
    virtual ~StateListener() {};

    /**
     * Called from an Engine instance whenever the state of the engine has
     * changed.
     *
     * @see Engine#registerStateListener()
     *
     * @param nNewState new state of the engine @see Engine#State
     */
    virtual void onStateChanged(const EngineState::type nNewState) = 0;
  };

}  // namespace TouchlessA3D

#endif
