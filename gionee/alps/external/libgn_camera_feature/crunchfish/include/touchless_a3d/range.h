#ifndef TA3D_RANGE_H
#define TA3D_RANGE_H

namespace TouchlessA3D {
  /**
   * A range defined by two iterators.
   *
   * @tparam IteratorType The type of the iterators. It must be copyable.
   */
  template<typename IteratorType>
  class Range {
  public:
    typedef IteratorType iterator;

    /**
     * Constructs a new range from two iterators.
     *
     * @param begin The beginning of the range.
     * @param end The end of the range.
     */
    Range(const IteratorType& begin, const IteratorType& end) :
      m_begin(begin),
      m_end(end)
    {
    }

    /**
     * Copies this range.
     */
    Range(const Range& other) :
      m_begin(other.m_begin),
      m_end(other.m_end)
    {
    }

    /**
     * Assigns to the range.
     */
    Range& operator=(const Range& other) {
      m_begin = other.m_begin;
      m_end = other.m_end;
    }

    /**
     * Returns the beginning of the range.
     *
     * @return The beginning of the range.
     */
    const IteratorType& begin() const {
      return m_begin;
    }

    /**
     * Returns the end of the range.
     *
     * @return The end of the range.
     */
    const IteratorType& end() const {
      return m_end;
    }

  private:
    IteratorType m_begin;
    IteratorType m_end;
  };
}

#endif  // TA3D_RANGE_H
