#!/system/bin/sh

touch /data/aurora/rootstatus
if [ ! -f /data/aurora/md5_original.file ];then
    echo "wait for create original md5 file,exit."
    echo -1 > /data/aurora/rootstatus
fi

/system/bin/sh /system/etc/md5_current.sh

tmp_current=`md5 /data/aurora/md5_current.file`
md5_current=${tmp_current:0:32}

tmp_original=`md5 /data/aurora/md5_original.file`
md5_original=${tmp_original:0:32}

echo "md5_current :"${md5_current}
echo "md5_original:"${md5_original}

if [[ ${md5_current} != ${md5_original} ]];then
    echo 1 > /data/aurora/rootstatus
else
    
    echo 0 > /data/aurora/rootstatus
fi
chmod 777 /data/aurora/rootstatus
sync
