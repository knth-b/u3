/*
 * Copyright (C) 2008 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.android.internal.policy.impl;


import com.android.internal.app.AlertController;
import com.android.internal.app.AlertController.AlertParams;
import com.android.internal.telephony.TelephonyIntents;
import com.android.internal.telephony.TelephonyProperties;
import com.android.internal.R;
import android.app.ActivityManagerNative;
import android.app.AlertDialog;
import android.app.Dialog;
import android.app.KeyguardManager;
import android.content.ActivityNotFoundException;
import android.content.BroadcastReceiver;
import android.content.ContentResolver;
import android.content.ContentValues;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.pm.UserInfo;
import android.database.ContentObserver;
import android.graphics.Color;
import android.graphics.PixelFormat;
import android.graphics.drawable.AnimationDrawable;
import android.graphics.drawable.Drawable;
import android.media.AudioManager;
import android.net.ConnectivityManager;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.os.RemoteException;
import android.os.ServiceManager;
import android.os.SystemClock;
import android.os.SystemProperties;
import android.os.UserHandle;
import android.os.UserManager;
import android.os.Vibrator;
import android.provider.Settings;
import android.service.dreams.DreamService;
import android.service.dreams.IDreamManager;
import android.telephony.PhoneStateListener;
import android.telephony.ServiceState;
import android.telephony.TelephonyManager;
import android.util.Log;
import android.util.TypedValue;
import android.view.InputDevice;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnLongClickListener;
import android.view.ViewConfiguration;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.view.WindowManagerPolicy.WindowManagerFuncs;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.view.animation.Animation.AnimationListener;
import android.widget.AdapterView;
import android.widget.BaseAdapter;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.ImageView.ScaleType;
import android.widget.LinearLayout.LayoutParams;
import android.widget.ListView;
import android.widget.TextView;
import android.app.Notification;
import android.app.NotificationManager;
import android.app.Notification.Builder;
import java.util.ArrayList;
import java.util.List;

import java.util.HashMap;
import android.media.AudioManager;
import android.media.SoundPool;
import android.app.AlarmManager;
import android.media.AudioManager;
import android.media.MediaPlayer;
import android.media.MediaPlayer.OnCompletionListener;
import android.media.SoundPool;

/**
 * Helper to show the global actions dialog. Each item is an {@link Action} that
 * may show depending on whether the keyguard is showing, and whether the device
 * is provisioned.
 */
class GlobalActions implements DialogInterface.OnDismissListener, DialogInterface.OnClickListener {

    private static final String TAG = "GlobalActions";

    private static final boolean SHOW_SILENT_TOGGLE = true;

    private final Context mContext;
    private final WindowManagerFuncs mWindowManagerFuncs;
    private final AudioManager mAudioManager;
    private final IDreamManager mDreamManager;

    private ArrayList<Action> mItems;
    private  AuroraGlobalActionsDialog mDialog;

    private Action mSilentModeAction;
    private ToggleAction mAirplaneModeOn;

    private MyAdapter mAdapter;

    private boolean mKeyguardShowing = false;
    private boolean mDeviceProvisioned = false;
    private ToggleAction.State mAirplaneState = ToggleAction.State.Off;
    private boolean mIsWaitingForEcmExit = false;
    private boolean mHasTelephony;
    private boolean mHasVibrator;
    private final boolean mShowSilentToggle;
    /**
     * @param context everything needs a context :(
     */
    public GlobalActions(Context context, WindowManagerFuncs windowManagerFuncs) {
        mContext = context;
        mWindowManagerFuncs = windowManagerFuncs;
        mAudioManager = (AudioManager) mContext.getSystemService(Context.AUDIO_SERVICE);
        mDreamManager = IDreamManager.Stub.asInterface(
                ServiceManager.getService(DreamService.DREAM_SERVICE));

        // receive broadcasts
        IntentFilter filter = new IntentFilter();
        filter.addAction(Intent.ACTION_CLOSE_SYSTEM_DIALOGS);
        filter.addAction(Intent.ACTION_SCREEN_OFF);
        filter.addAction(TelephonyIntents.ACTION_EMERGENCY_CALLBACK_MODE_CHANGED);
        context.registerReceiver(mBroadcastReceiver, filter);

        // get notified of phone state changes
        TelephonyManager telephonyManager =
                (TelephonyManager) context.getSystemService(Context.TELEPHONY_SERVICE);
        telephonyManager.listen(mPhoneStateListener, PhoneStateListener.LISTEN_SERVICE_STATE);
        ConnectivityManager cm = (ConnectivityManager)
                context.getSystemService(Context.CONNECTIVITY_SERVICE);
        mHasTelephony = cm.isNetworkSupported(ConnectivityManager.TYPE_MOBILE);
        mContext.getContentResolver().registerContentObserver(
                Settings.Global.getUriFor(Settings.Global.AIRPLANE_MODE_ON), true,
                mAirplaneModeObserver);
        Vibrator vibrator = (Vibrator) mContext.getSystemService(Context.VIBRATOR_SERVICE);
        mHasVibrator = vibrator != null && vibrator.hasVibrator();

        mShowSilentToggle = SHOW_SILENT_TOGGLE && !mContext.getResources().getBoolean(
                com.android.internal.R.bool.config_useFixedVolume);
    }

    /**
     * Show the global actions dialog (creating if necessary)
     * 
     * @param keyguardShowing True if keyguard is showing
     */
    public void showDialog(boolean keyguardShowing, boolean isDeviceProvisioned) {
        mKeyguardShowing = keyguardShowing;
        mDeviceProvisioned = isDeviceProvisioned;
        if (mDialog != null) {
            mDialog.dismiss();
            mDialog = null;
            // Show delayed, so that the dismiss of the previous dialog
            // completes
            mHandler.sendEmptyMessage(MESSAGE_SHOW);
        } else {
            handleShow();
        }
    }

    private void awakenIfNecessary() {
        if (mDreamManager != null) {
            try {
                if (mDreamManager.isDreaming()) {
                    mDreamManager.awaken();
                }
            } catch (RemoteException e) {
                // we tried
            }
        }
    }

    private void handleShow() {
        awakenIfNecessary();
        long startTime = System.currentTimeMillis();
        mDialog = createDialog();
        long middleTime = System.currentTimeMillis();
        Log.e("time", "middle Time:"+(middleTime - startTime));
        prepareDialog();

        WindowManager.LayoutParams attrs = mDialog.getWindow().getAttributes();
        attrs.setTitle("GlobalActions");
        // attrs.format = PixelFormat.TRANSLUCENT;
        // attrs.alpha=0.85f;
        mDialog.getWindow().setWindowAnimations(com.aurora.R.style.AuroraShutdownDialogAnim);
        mDialog.getWindow().setAttributes(attrs);
       
        
        mDialog.show();
//        mContext.startService(new Intent("com.aurora.internal.view.ShutdownService"));
        mDialog.getWindow().getDecorView().setSystemUiVisibility(View.STATUS_BAR_DISABLE_EXPAND);
    }

    /**
     * Create the global actions dialog.
     * 
     * @return A new dialog.
     */
    private AuroraGlobalActionsDialog createDialog() {
        // Simple toggle style if there's no vibrator, otherwise use a tri-state
    	AuroraGlobalActionsDialog auroraDialog = new AuroraGlobalActionsDialog(mContext);
    	if(auroraDialog != null){
    		return auroraDialog;
    	}
        if (!mHasVibrator) {
            mSilentModeAction = new SilentModeToggleAction();
        } else {
            mSilentModeAction = new SilentModeTriStateAction(mContext, mAudioManager, mHandler);
        }
        
        mAirplaneModeOn = new ToggleAction(
                R.drawable.ic_lock_airplane_mode,
                R.drawable.ic_lock_airplane_mode_off,
                R.string.global_actions_toggle_airplane_mode,
                R.string.global_actions_airplane_mode_on_status,
                R.string.global_actions_airplane_mode_off_status) {

            void onToggle(boolean on) {
                if (mHasTelephony && Boolean.parseBoolean(
                        SystemProperties.get(TelephonyProperties.PROPERTY_INECM_MODE))) {
                    mIsWaitingForEcmExit = true;
                    // Launch ECM exit dialog
                    Intent ecmDialogIntent =
                            new Intent(TelephonyIntents.ACTION_SHOW_NOTICE_ECM_BLOCK_OTHERS, null);
                    ecmDialogIntent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                    mContext.startActivity(ecmDialogIntent);
                } else {
                    changeAirplaneModeSystemSetting(on);
                }
            }

            @Override
            protected void changeStateFromPress(boolean buttonOn) {
                if (!mHasTelephony)
                    return;

                // In ECM mode airplane state cannot be changed
                if (!(Boolean.parseBoolean(
                        SystemProperties.get(TelephonyProperties.PROPERTY_INECM_MODE)))) {
                    mState = buttonOn ? State.TurningOn : State.TurningOff;
                    mAirplaneState = mState;
                }
            }

            public boolean showDuringKeyguard() {
                return true;
            }

            public boolean showBeforeProvisioning() {
                return false;
            }
        };
        onAirplaneModeChanged();

        mItems = new ArrayList<Action>();

        final boolean quickbootEnabled = Settings.System.getInt(
                mContext.getContentResolver(), "enable_quickboot", 0) == 1;
        // first: power off
        mItems.add(
                new SinglePressAction(
                        com.android.internal.R.drawable.ic_lock_power_off,
                        R.string.global_action_power_off) {

                public void onPress() {
                    // goto quickboot mode
                    if (quickbootEnabled) {
                        startQuickBoot();
                        return;
                    }

                    // shutdown by making sure radio and power are handled accordingly.
                    mWindowManagerFuncs.shutdown(true);
                }

                    public boolean onLongPress() {
                        mWindowManagerFuncs.rebootSafeMode(true);
                        return true;
                    }

                    public boolean showDuringKeyguard() {
                        return true;
                    }

                    public boolean showBeforeProvisioning() {
                        return true;
                    }
                });

        // next: airplane mode
        mItems.add(mAirplaneModeOn);

        // next: bug report, if enabled
        if (Settings.Global.getInt(mContext.getContentResolver(),
                Settings.Global.BUGREPORT_IN_POWER_MENU, 0) != 0 && isCurrentUserOwner()) {
            mItems.add(
                    new SinglePressAction(com.android.internal.R.drawable.stat_sys_adb,
                            R.string.global_action_bug_report) {

                        public void onPress() {
                            AlertDialog.Builder builder = new AlertDialog.Builder(mContext);
                            builder.setTitle(com.android.internal.R.string.bugreport_title);
                            builder.setMessage(com.android.internal.R.string.bugreport_message);
                            builder.setNegativeButton(com.android.internal.R.string.cancel, null);
                            builder.setPositiveButton(com.android.internal.R.string.report,
                                    new DialogInterface.OnClickListener() {
                                        @Override
                                        public void onClick(DialogInterface dialog, int which) {
                                            // Add a little delay before
                                            // executing, to give the
                                            // dialog a chance to go away before
                                            // it takes a
                                            // screenshot.
                                            mHandler.postDelayed(new Runnable() {
                                                @Override
                                                public void run() {
                                                    try {
                                                        ActivityManagerNative.getDefault()
                                                                .requestBugReport();
                                                    } catch (RemoteException e) {
                                                    }
                                                }
                                            }, 500);
                                        }
                                    });
                            AlertDialog dialog = builder.create();
                            dialog.getWindow().setType(
                                    WindowManager.LayoutParams.TYPE_KEYGUARD_DIALOG);
                            dialog.show();
                        }

                        public boolean onLongPress() {
                            return false;
                        }

                        public boolean showDuringKeyguard() {
                            return true;
                        }

                        public boolean showBeforeProvisioning() {
                            return false;
                        }
                    });
        }

        // last: silent mode
        if (mShowSilentToggle) {
            mItems.add(mSilentModeAction);
        }

        // one more thing: optionally add a list of users to switch to
        if (SystemProperties.getBoolean("fw.power_user_switcher", false)) {
            addUsersToMenu(mItems);
        }

        mAdapter = new MyAdapter();
        /*
         * AlertParams params = new AlertParams(mContext); params.mAdapter =
         * mAdapter; params.mOnClickListener = this;
         * params.mForceInverseBackground = true; GlobalActionsDialog dialog =
         * new GlobalActionsDialog(mContext, params);
         * dialog.setCanceledOnTouchOutside(false); // Handled by the custom
         * class. dialog.getListView().setItemsCanFocus(true);
         * dialog.getListView().setLongClickable(true);
         * dialog.getListView().setOnItemLongClickListener( new
         * AdapterView.OnItemLongClickListener() {
         * @Override public boolean onItemLongClick(AdapterView<?> parent, View
         * view, int position, long id) { return
         * mAdapter.getItem(position).onLongPress(); } });
         * dialog.getWindow().setType
         * (WindowManager.LayoutParams.TYPE_KEYGUARD_DIALOG);
         * dialog.setOnDismissListener(this);
         */
        //globalac
        // Aurora <Luofu> <2013-11-14> modify for shutdow begin
//        AuroraGlobalActionsDialog auroraDialog = new AuroraGlobalActionsDialog(mContext);
        // Aurora <Luofu> <2013-11-14> modify for shutdow end
        return auroraDialog;
    }

    private UserInfo getCurrentUser() {
        try {
            return ActivityManagerNative.getDefault().getCurrentUser();
        } catch (RemoteException re) {
            return null;
        }
    }

    private boolean isCurrentUserOwner() {
        UserInfo currentUser = getCurrentUser();
        return currentUser == null || currentUser.isPrimary();
    }

    private void addUsersToMenu(ArrayList<Action> items) {
        List<UserInfo> users = ((UserManager) mContext.getSystemService(Context.USER_SERVICE))
                .getUsers();
        if (users.size() > 1) {
            UserInfo currentUser = getCurrentUser();
            for (final UserInfo user : users) {
                boolean isCurrentUser = currentUser == null
                        ? user.id == 0 : (currentUser.id == user.id);
                Drawable icon = user.iconPath != null ? Drawable.createFromPath(user.iconPath)
                        : null;
                SinglePressAction switchToUser = new SinglePressAction(
                        com.android.internal.R.drawable.ic_menu_cc, icon,
                        (user.name != null ? user.name : "Primary")
                                + (isCurrentUser ? " \u2714" : "")) {
                    public void onPress() {
                        try {
                            ActivityManagerNative.getDefault().switchUser(user.id);
                        } catch (RemoteException re) {
                            Log.e(TAG, "Couldn't switch user " + re);
                        }
                    }

                    public boolean showDuringKeyguard() {
                        return true;
                    }

                    public boolean showBeforeProvisioning() {
                        return false;
                    }
                };
                items.add(switchToUser);
            }
        }
    }

    private void prepareDialog() {
//        refreshSilentMode();
//        mAirplaneModeOn.updateState(mAirplaneState);
//        mAdapter.notifyDataSetChanged();
        mDialog.getWindow().setType(WindowManager.LayoutParams.TYPE_KEYGUARD_DIALOG);
//        if (mShowSilentToggle) {
//            IntentFilter filter = new IntentFilter(AudioManager.RINGER_MODE_CHANGED_ACTION);
//            mContext.registerReceiver(mRingerModeReceiver, filter);
//        }
    }

    private void refreshSilentMode() {
//        if (!mHasVibrator) {
//            final boolean silentModeOn =
//                    mAudioManager.getRingerMode() != AudioManager.RINGER_MODE_NORMAL;
//            ((ToggleAction) mSilentModeAction).updateState(
//                    silentModeOn ? ToggleAction.State.On : ToggleAction.State.Off);
//        }
    }

    /** {@inheritDoc} */
    public void onDismiss(DialogInterface dialog) {
//        if (mShowSilentToggle) {
//            try {
//                mContext.unregisterReceiver(mRingerModeReceiver);
//            } catch (IllegalArgumentException ie) {
//                // ignore this
//                Log.w(TAG, ie);
//            }
//        }
    }

    /** {@inheritDoc} */
    public void onClick(DialogInterface dialog, int which) {
//        if (!(mAdapter.getItem(which) instanceof SilentModeTriStateAction)) {
//            dialog.dismiss();
//        }
//        mAdapter.getItem(which).onPress();
    }

    /**
     * The adapter used for the list within the global actions dialog, taking
     * into account whether the keyguard is showing via
     * {@link GlobalActions#mKeyguardShowing} and whether the device is
     * provisioned via {@link GlobalActions#mDeviceProvisioned}.
     */
    private class MyAdapter extends BaseAdapter {

        public int getCount() {
            int count = 0;

            for (int i = 0; i < mItems.size(); i++) {
                final Action action = mItems.get(i);

                if (mKeyguardShowing && !action.showDuringKeyguard()) {
                    continue;
                }
                if (!mDeviceProvisioned && !action.showBeforeProvisioning()) {
                    continue;
                }
                count++;
            }
            return count;
        }

        @Override
        public boolean isEnabled(int position) {
            return getItem(position).isEnabled();
        }

        @Override
        public boolean areAllItemsEnabled() {
            return false;
        }

        public Action getItem(int position) {

            int filteredPos = 0;
            for (int i = 0; i < mItems.size(); i++) {
                final Action action = mItems.get(i);
                if (mKeyguardShowing && !action.showDuringKeyguard()) {
                    continue;
                }
                if (!mDeviceProvisioned && !action.showBeforeProvisioning()) {
                    continue;
                }
                if (filteredPos == position) {
                    return action;
                }
                filteredPos++;
            }

            throw new IllegalArgumentException("position " + position
                    + " out of range of showable actions"
                    + ", filtered count=" + getCount()
                    + ", keyguardshowing=" + mKeyguardShowing
                    + ", provisioned=" + mDeviceProvisioned);
        }

        public long getItemId(int position) {
            return position;
        }

        public View getView(int position, View convertView, ViewGroup parent) {
            Action action = getItem(position);
            return action.create(mContext, convertView, parent, LayoutInflater.from(mContext));
        }
    }

    // note: the scheme below made more sense when we were planning on having
    // 8 different things in the global actions dialog. seems overkill with
    // only 3 items now, but may as well keep this flexible approach so it will
    // be easy should someone decide at the last minute to include something
    // else, such as 'enable wifi', or 'enable bluetooth'

    /**
     * What each item in the global actions dialog must be able to support.
     */
    private interface Action {
        View create(Context context, View convertView, ViewGroup parent, LayoutInflater inflater);

        void onPress();

        public boolean onLongPress();

        /**
         * @return whether this action should appear in the dialog when the
         *         keygaurd is showing.
         */
        boolean showDuringKeyguard();

        /**
         * @return whether this action should appear in the dialog before the
         *         device is provisioned.
         */
        boolean showBeforeProvisioning();

        boolean isEnabled();
    }

    /**
     * A single press action maintains no state, just responds to a press and
     * takes an action.
     */
    private static abstract class SinglePressAction implements Action {
        private final int mIconResId;
        private final Drawable mIcon;
        private final int mMessageResId;
        private final CharSequence mMessage;

        protected SinglePressAction(int iconResId, int messageResId) {
            mIconResId = iconResId;
            mMessageResId = messageResId;
            mMessage = null;
            mIcon = null;
        }

        protected SinglePressAction(int iconResId, Drawable icon, CharSequence message) {
            mIconResId = iconResId;
            mMessageResId = 0;
            mMessage = message;
            mIcon = icon;
        }

        protected SinglePressAction(int iconResId, CharSequence message) {
            mIconResId = iconResId;
            mMessageResId = 0;
            mMessage = message;
            mIcon = null;
        }

        public boolean isEnabled() {
            return true;
        }

        abstract public void onPress();

        public boolean onLongPress() {
            return false;
        }

        public View create(
                Context context, View convertView, ViewGroup parent, LayoutInflater inflater) {
            View v = inflater.inflate(R.layout.global_actions_item, parent, false);

            ImageView icon = (ImageView) v.findViewById(R.id.icon);
            TextView messageView = (TextView) v.findViewById(R.id.message);

            v.findViewById(R.id.status).setVisibility(View.GONE);
            if (mIcon != null) {
                icon.setImageDrawable(mIcon);
                icon.setScaleType(ScaleType.CENTER_CROP);
            } else if (mIconResId != 0) {
                icon.setImageDrawable(context.getResources().getDrawable(mIconResId));
            }
            if (mMessage != null) {
                messageView.setText(mMessage);
            } else {
                messageView.setText(mMessageResId);
            }

            return v;
        }
    }

    /**
     * A toggle action knows whether it is on or off, and displays an icon and
     * status message accordingly.
     */
    private static abstract class ToggleAction implements Action {

        enum State {
            Off(false),
            TurningOn(true),
            TurningOff(true),
            On(false);

            private final boolean inTransition;

            State(boolean intermediate) {
                inTransition = intermediate;
            }

            public boolean inTransition() {
                return inTransition;
            }
        }

        protected State mState = State.Off;

        // prefs
        protected int mEnabledIconResId;
        protected int mDisabledIconResid;
        protected int mMessageResId;
        protected int mEnabledStatusMessageResId;
        protected int mDisabledStatusMessageResId;

        /**
         * @param enabledIconResId The icon for when this action is on.
         * @param disabledIconResid The icon for when this action is off.
         * @param essage The general information message, e.g 'Silent Mode'
         * @param enabledStatusMessageResId The on status message, e.g 'sound
         *            disabled'
         * @param disabledStatusMessageResId The off status message, e.g. 'sound
         *            enabled'
         */
        public ToggleAction(int enabledIconResId,
                int disabledIconResid,
                int message,
                int enabledStatusMessageResId,
                int disabledStatusMessageResId) {
            mEnabledIconResId = enabledIconResId;
            mDisabledIconResid = disabledIconResid;
            mMessageResId = message;
            mEnabledStatusMessageResId = enabledStatusMessageResId;
            mDisabledStatusMessageResId = disabledStatusMessageResId;
        }

        /**
         * Override to make changes to resource IDs just before creating the
         * View.
         */
        void willCreate() {

        }

        public View create(Context context, View convertView, ViewGroup parent,
                LayoutInflater inflater) {
            willCreate();

            View v = inflater.inflate(R
                    .layout.global_actions_item, parent, false);

            ImageView icon = (ImageView) v.findViewById(R.id.icon);
            TextView messageView = (TextView) v.findViewById(R.id.message);
            TextView statusView = (TextView) v.findViewById(R.id.status);
            final boolean enabled = isEnabled();

            if (messageView != null) {
                messageView.setText(mMessageResId);
                messageView.setEnabled(enabled);
            }

            boolean on = ((mState == State.On) || (mState == State.TurningOn));
            if (icon != null) {
                icon.setImageDrawable(context.getResources().getDrawable(
                        (on ? mEnabledIconResId : mDisabledIconResid)));
                icon.setEnabled(enabled);
            }

            if (statusView != null) {
                statusView.setText(on ? mEnabledStatusMessageResId : mDisabledStatusMessageResId);
                statusView.setVisibility(View.VISIBLE);
                statusView.setEnabled(enabled);
            }
            v.setEnabled(enabled);

            return v;
        }

        public final void onPress() {
            if (mState.inTransition()) {
                Log.w(TAG, "shouldn't be able to toggle when in transition");
                return;
            }

            final boolean nowOn = !(mState == State.On);
            onToggle(nowOn);
            changeStateFromPress(nowOn);
        }

        public boolean onLongPress() {
            return false;
        }

        public boolean isEnabled() {
            return !mState.inTransition();
        }

        /**
         * Implementations may override this if their state can be in on of the
         * intermediate states until some notification is received (e.g airplane
         * mode is 'turning off' until we know the wireless connections are back
         * online
         * 
         * @param buttonOn Whether the button was turned on or off
         */
        protected void changeStateFromPress(boolean buttonOn) {
            mState = buttonOn ? State.On : State.Off;
        }

        abstract void onToggle(boolean on);

        public void updateState(State state) {
            mState = state;
        }
    }

    private class SilentModeToggleAction extends ToggleAction {
        public SilentModeToggleAction() {
            super(R.drawable.ic_audio_vol_mute,
                    R.drawable.ic_audio_vol,
                    R.string.global_action_toggle_silent_mode,
                    R.string.global_action_silent_mode_on_status,
                    R.string.global_action_silent_mode_off_status);
        }

        void onToggle(boolean on) {
            if (on) {
                mAudioManager.setRingerMode(AudioManager.RINGER_MODE_SILENT);
            } else {
                mAudioManager.setRingerMode(AudioManager.RINGER_MODE_NORMAL);
            }
        }

        public boolean showDuringKeyguard() {
            return true;
        }

        public boolean showBeforeProvisioning() {
            return false;
        }
    }

    private static class SilentModeTriStateAction implements Action, View.OnClickListener {

        private final int[] ITEM_IDS = {
                R.id.option1, R.id.option2, R.id.option3
        };

        private final AudioManager mAudioManager;
        private final Handler mHandler;
        private final Context mContext;

        SilentModeTriStateAction(Context context, AudioManager audioManager, Handler handler) {
            mAudioManager = audioManager;
            mHandler = handler;
            mContext = context;
        }

        private int ringerModeToIndex(int ringerMode) {
            // They just happen to coincide
            return ringerMode;
        }

        private int indexToRingerMode(int index) {
            // They just happen to coincide
            return index;
        }

        public View create(Context context, View convertView, ViewGroup parent,
                LayoutInflater inflater) {
            View v = inflater.inflate(R.layout.global_actions_silent_mode, parent, false);

            int selectedIndex = ringerModeToIndex(mAudioManager.getRingerMode());
            for (int i = 0; i < 3; i++) {
                View itemView = v.findViewById(ITEM_IDS[i]);
                itemView.setSelected(selectedIndex == i);
                // Set up click handler
                itemView.setTag(i);
                itemView.setOnClickListener(this);
            }
            return v;
        }

        public void onPress() {
        }

        public boolean onLongPress() {
            return false;
        }

        public boolean showDuringKeyguard() {
            return true;
        }

        public boolean showBeforeProvisioning() {
            return false;
        }

        public boolean isEnabled() {
            return true;
        }

        void willCreate() {
        }

        public void onClick(View v) {
            if (!(v.getTag() instanceof Integer))
                return;

            int index = (Integer) v.getTag();
            mAudioManager.setRingerMode(indexToRingerMode(index));
            mHandler.sendEmptyMessageDelayed(MESSAGE_DISMISS, DIALOG_DISMISS_DELAY);
        }
    }

    private BroadcastReceiver mBroadcastReceiver = new BroadcastReceiver() {
        public void onReceive(Context context, Intent intent) {
            String action = intent.getAction();
            if (Intent.ACTION_CLOSE_SYSTEM_DIALOGS.equals(action)
                    || Intent.ACTION_SCREEN_OFF.equals(action)) {
                String reason = intent.getStringExtra(PhoneWindowManager.SYSTEM_DIALOG_REASON_KEY);
                if (!PhoneWindowManager.SYSTEM_DIALOG_REASON_GLOBAL_ACTIONS.equals(reason)) {
                    mHandler.sendEmptyMessage(MESSAGE_DISMISS);
                }
            } else if (TelephonyIntents.ACTION_EMERGENCY_CALLBACK_MODE_CHANGED.equals(action)) {
                // Airplane mode can be changed after ECM exits if airplane
                // toggle button
                // is pressed during ECM mode
                if (!(intent.getBooleanExtra("PHONE_IN_ECM_STATE", false)) &&
                        mIsWaitingForEcmExit) {
                    mIsWaitingForEcmExit = false;
                    changeAirplaneModeSystemSetting(true);
                }
            }
        }
    };

    PhoneStateListener mPhoneStateListener = new PhoneStateListener() {
        @Override
        public void onServiceStateChanged(ServiceState serviceState) {
            if (!mHasTelephony)
                return;
            final boolean inAirplaneMode = serviceState.getState() == ServiceState.STATE_POWER_OFF;
            mAirplaneState = inAirplaneMode ? ToggleAction.State.On : ToggleAction.State.Off;
            //aurora modify liguangyu 20140605 for BUG #5379 start
		    if(mAirplaneModeOn != null) {
	            mAirplaneModeOn.updateState(mAirplaneState);
		    }
		    if(mAdapter != null) {
		    	mAdapter.notifyDataSetChanged();
		    }
            //aurora modify liguangyu 20140605 for BUG #5379 end
        }
    };

    private BroadcastReceiver mRingerModeReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            if (intent.getAction().equals(AudioManager.RINGER_MODE_CHANGED_ACTION)) {
                mHandler.sendEmptyMessage(MESSAGE_REFRESH);
            }
        }
    };

    private ContentObserver mAirplaneModeObserver = new ContentObserver(new Handler()) {
        @Override
        public void onChange(boolean selfChange) {
            onAirplaneModeChanged();
        }
    };

    private static final int MESSAGE_DISMISS = 0;
    private static final int MESSAGE_REFRESH = 1;
    private static final int MESSAGE_SHOW = 2;
    private static final int MESSAGE_REFRESH_AIRPLANEMODE = 3;
    private static final int DIALOG_DISMISS_DELAY = 300; // ms

    private Handler mHandler = new Handler() {
        public void handleMessage(Message msg) {
            switch (msg.what) {
                case MESSAGE_DISMISS:
                    if (mDialog != null) {
                        mDialog.dismiss();
                    }
                    break;
                case MESSAGE_REFRESH:
                    refreshSilentMode();
                    mAdapter.notifyDataSetChanged();
                    break;
                case MESSAGE_SHOW:
                    handleShow();
                    break;
            case MESSAGE_REFRESH_AIRPLANEMODE:
                mAirplaneModeOn.updateState(mAirplaneState);
                mAdapter.notifyDataSetChanged();
                break;
            }
        }
    };

    private void onAirplaneModeChanged() {
        // Let the service state callbacks handle the state.
        if (mHasTelephony)
            return;

        boolean airplaneModeOn = Settings.Global.getInt(
                mContext.getContentResolver(),
                Settings.Global.AIRPLANE_MODE_ON,
                0) == 1;
        mAirplaneState = airplaneModeOn ? ToggleAction.State.On : ToggleAction.State.Off;
        mAirplaneModeOn.updateState(mAirplaneState);
    }

    /**
     * Change the airplane mode system setting
     */
    private void changeAirplaneModeSystemSetting(boolean on) {
        Settings.Global.putInt(
                mContext.getContentResolver(),
                Settings.Global.AIRPLANE_MODE_ON,
                on ? 1 : 0);
        Intent intent = new Intent(Intent.ACTION_AIRPLANE_MODE_CHANGED);
        intent.addFlags(Intent.FLAG_RECEIVER_REPLACE_PENDING);
        intent.putExtra("state", on);
        mContext.sendBroadcastAsUser(intent, UserHandle.ALL);
        if (!mHasTelephony) {
            mAirplaneState = on ? ToggleAction.State.On : ToggleAction.State.Off;
        }
    }

    private void startQuickBoot() {

        Intent intent = new Intent("org.codeaurora.action.QUICKBOOT");
        intent.putExtra("mode", 0);
        try {
            mContext.startActivityAsUser(intent,UserHandle.CURRENT);
        } catch (ActivityNotFoundException e) {
        }
    }

    private static final class GlobalActionsDialog extends Dialog implements DialogInterface {
        private final Context mContext;
        private final int mWindowTouchSlop;
        private final AlertController mAlert;

        private EnableAccessibilityController mEnableAccessibilityController;

        private boolean mIntercepted;
        private boolean mCancelOnUp;

        public GlobalActionsDialog(Context context, AlertParams params) {
            super(context, getDialogTheme(context));
            mContext = context;
            mAlert = new AlertController(mContext, this, getWindow());
            mWindowTouchSlop = ViewConfiguration.get(context).getScaledWindowTouchSlop();
            params.apply(mAlert);
        }

        private static int getDialogTheme(Context context) {
            TypedValue outValue = new TypedValue();
            context.getTheme().resolveAttribute(com.android.internal.R.attr.alertDialogTheme,
                    outValue, true);
            return outValue.resourceId;
        }

        @Override
        protected void onStart() {
            // If global accessibility gesture can be performed, we will take
            // care
            // of dismissing the dialog on touch outside. This is because the
            // dialog
            // is dismissed on the first down while the global gesture is a long
            // press
            // with two fingers anywhere on the screen.
            if (EnableAccessibilityController.canEnableAccessibilityViaGesture(mContext)) {
                mEnableAccessibilityController = new EnableAccessibilityController(mContext);
                super.setCanceledOnTouchOutside(false);
            } else {
                mEnableAccessibilityController = null;
                super.setCanceledOnTouchOutside(true);
            }
            super.onStart();
        }

        @Override
        protected void onStop() {
            if (mEnableAccessibilityController != null) {
                mEnableAccessibilityController.onDestroy();
            }
            super.onStop();
        }

        @Override
        public boolean dispatchTouchEvent(MotionEvent event) {
            if (mEnableAccessibilityController != null) {
                final int action = event.getActionMasked();
                if (action == MotionEvent.ACTION_DOWN) {
                    View decor = getWindow().getDecorView();
                    final int eventX = (int) event.getX();
                    final int eventY = (int) event.getY();
                    if (eventX < -mWindowTouchSlop
                            || eventY < -mWindowTouchSlop
                            || eventX >= decor.getWidth() + mWindowTouchSlop
                            || eventY >= decor.getHeight() + mWindowTouchSlop) {
                        mCancelOnUp = true;
                    }
                }
                try {
                    if (!mIntercepted) {
                        mIntercepted = mEnableAccessibilityController.onInterceptTouchEvent(event);
                        if (mIntercepted) {
                            final long now = SystemClock.uptimeMillis();
                            event = MotionEvent.obtain(now, now,
                                    MotionEvent.ACTION_CANCEL, 0.0f, 0.0f, 0);
                            event.setSource(InputDevice.SOURCE_TOUCHSCREEN);
                            mCancelOnUp = true;
                        }
                    } else {
                        return mEnableAccessibilityController.onTouchEvent(event);
                    }
                } finally {
                    if (action == MotionEvent.ACTION_UP) {
                        if (mCancelOnUp) {
                            cancel();
                        }
                        mCancelOnUp = false;
                        mIntercepted = false;
                    }
                }
            }
            return super.dispatchTouchEvent(event);
        }

        public ListView getListView() {
            return mAlert.getListView();
        }

        @Override
        protected void onCreate(Bundle savedInstanceState) {
            super.onCreate(savedInstanceState);
            mAlert.installContent();
        }

        @Override
        public boolean onKeyDown(int keyCode, KeyEvent event) {
            if (mAlert.onKeyDown(keyCode, event)) {
                return true;
            }
            return super.onKeyDown(keyCode, event);
        }

        @Override
        public boolean onKeyUp(int keyCode, KeyEvent event) {
            if (mAlert.onKeyUp(keyCode, event)) {
                return true;
            }
            return super.onKeyUp(keyCode, event);
        }
    }

    private final class AuroraGlobalActionsDialog extends Dialog implements DialogInterface,
 android.view.View.OnClickListener,OnLongClickListener, AnimationListener,AnimationImageListener{

		private final Context mContext;
		private int mWindowTouchSlop;
		// private GridView grid;

		private EnableAccessibilityController mEnableAccessibilityController;

		private boolean mIntercepted;
		private boolean mCancelOnUp;

		private ImageButton shutdown;
		private ImageButton reboot;
		private ImageButton airmode;

		private View layoutAirMode;
		private View layoutReboot;
		private View layoutShutdown;

		private View mNavView;

		private TextView textAirMode;
		private TextView textReboot;
		private TextView textShutdown;

		private AlertDialog mRebootConfirmAlert;

		/**
		 * 监听虚拟键状态的改变 通过 ContentObserver#onChange()获得 NAVI_KEY_HIDE 改变的情况
		 */
		private SettingsObserver mSettingsObserver;

		private int airmodeOpen;
		private int airmodeClose;

		private int airmodeOpenImg;
		private int airmodeCloseImg;

		private int mKeyBack = 0;

		private Animation airModeAnim;
		private Animation rebootAnim;
		private Animation shutdownAnim;

		private Animation mDismissAirMode;
		private Animation mDismissReboot;
		private Animation mDismissAnim;
		private Animation mAirmodeTextAnim;
		private Animation mShutdownTextAnim;
		private Animation mRebootTextAnim;
		
		private Animation mAirmodeTextDismissAnim;
		private Animation mShutdownTextDismissAnim;
		private Animation mRebootTextDismissAnim;

		private FrameAnimation airModeIconAnim;
		private FrameAnimation rebootIconAnim;
		private FrameAnimation shutdownIconAnim;
		private final int MSG_PLAY_REBOOT_ANIM = 100;
		private final int MSG_PLAY_SHUTDOWN_ANIM = 101;
		private final int MSG_PLAY_REBOOT_DISMISS = 102;
		private final int MSG_PLAY_SHUTDOWN_DISMISS = 103;
		private final int MSG_PLAY_ICON_ANIM = 104;
		private final int MSG_PLAY_TEXT_ANIM = 105;

		private final int NAV_MODE_CANCLE = -1;
		private final int NAV_MODE_SET_COLOR = 6;

		private final int mSlideToShowNavArea = 100;
		private final int mSlideDistance = 10;
		private final String NAV_COLOR = "#D9000000";

		private final String NAV_ACTION = "aurora.action.SET_NAVIBAR_COLOR";
		private static final String NAVI_KEY_HIDE = "navigation_key_hide";
		private int mScreenHeight;
		private int mTouchedY;
		private Handler mNavigationKeyHandler = new Handler();

		private Handler mAnimHandler = new Handler() {
			public void handleMessage(android.os.Message msg) {
				switch (msg.what) {
				case MSG_PLAY_REBOOT_ANIM:
					reboot.startAnimation(rebootAnim);
					layoutReboot.setVisibility(View.VISIBLE);
//					textReboot.startAnimation(mRebootTextAnim);
//					textReboot.setVisibility(View.VISIBLE);
					
					break;
				case MSG_PLAY_SHUTDOWN_ANIM:
					shutdown.startAnimation(shutdownAnim);
					layoutShutdown.setVisibility(View.VISIBLE);
//					textShutdown.startAnimation(mShutdownTextAnim);
//					textShutdown.setVisibility(View.VISIBLE);
					break;
				case MSG_PLAY_REBOOT_DISMISS:
					reboot.setAnimation(mDismissReboot);
					textReboot.startAnimation(mRebootTextDismissAnim);
                    break;
                case MSG_PLAY_SHUTDOWN_DISMISS:
                    shutdown.startAnimation(mDismissAnim);
//                    layoutShutdown.setVisibility(View.INVISIBLE);
                    textShutdown.startAnimation(mShutdownTextDismissAnim);
                    break;
				case MSG_PLAY_ICON_ANIM:
					airModeIconAnim.start();
					rebootIconAnim.start();
					shutdownIconAnim.start();
					break;

				case MSG_PLAY_TEXT_ANIM:
					textAirMode.startAnimation(mAirmodeTextAnim);
					textReboot.startAnimation(mRebootTextAnim);
					textShutdown.startAnimation(mShutdownTextAnim);
				break;
				default:
					break;
				}

			};

		};
		private boolean isAirMode = false;

		private LayoutInflater mInflater;
		private View mContentView;
		private String mSoundPath = "/system/media/audio/ui/PowerMenu.ogg";

		long startTime;
		long endTime;
		public AuroraGlobalActionsDialog(Context context) {
			super(context, com.aurora.R.style.Aurora_Dialog_Fullscreen);
			// TODO Auto-generated constructor stub
			mContext = context;
			mInflater = (LayoutInflater) mContext
					.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
			 startTime = System.currentTimeMillis();
			mContentView = mInflater.inflate(
					com.aurora.R.layout.aurora_power_layout, null);
			mContentView
			.setSystemUiVisibility(View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN
					| View.SYSTEM_UI_FLAG_LAYOUT_STABLE);
			mWindowTouchSlop = ViewConfiguration.get(mContext)
					.getScaledWindowTouchSlop();
			shutdown = (ImageButton) mContentView
					.findViewById(com.aurora.R.id.aurora_shutdown_btn);
			reboot = (ImageButton) mContentView
					.findViewById(com.aurora.R.id.aurora_reboot_btn);
			airmode = (ImageButton) mContentView
					.findViewById(com.aurora.R.id.aurora_airmode_btn);

			airmodeClose = com.aurora.R.string.aurora_airmode;
			airmodeOpen = com.aurora.R.string.aurora_airmode_open;
			airmodeOpenImg = com.aurora.R.drawable.aurora_airmode_normal;
			airmodeCloseImg = com.aurora.R.drawable.aurora_airmode_pressed;

			layoutAirMode = mContentView
					.findViewById(com.aurora.R.id.air_mode_layout);
			layoutReboot = mContentView
					.findViewById(com.aurora.R.id.reboot_layout);
			layoutShutdown = mContentView
					.findViewById(com.aurora.R.id.shutdown_layout);

			textAirMode = (TextView) mContentView
					.findViewById(com.aurora.R.id.aurora_airmode_text);
			textReboot = (TextView) mContentView
					.findViewById(com.aurora.R.id.aurora_reboot_text);
			textShutdown = (TextView) mContentView
					.findViewById(com.aurora.R.id.aurora_shutdown_text);

			mNavView = mContentView
					.findViewById(com.aurora.R.id.aurora_nav_buttons);
			initAnim();

		}

		@Override
		protected void onCreate(Bundle savedInstanceState) {
			super.onCreate(savedInstanceState);
			// getWindow().getAttributes()
			setContentView(mContentView);
			
			initRebootConfirmAlert();
			int isAirplaneMode = Settings.System.getInt(
					mContext.getContentResolver(),
					Settings.System.AIRPLANE_MODE_ON, 0);
			boolean find = (isAirplaneMode == 1) ? true : false;
			isAirMode = find;

			textAirMode.setText(isAirMode ? airmodeClose : airmodeOpen);

			shutdown.setOnClickListener(this);
			reboot.setOnClickListener(this);
			airmode.setOnClickListener(this);
			shutdown.setOnLongClickListener(this);

		}

		/**
		 * 通过改写 NAVI_KEY_HIDE 的值，可以控制虚拟键的显示/隐藏。 hide = true, 写入1，代表隐藏虚拟键 hide =
		 * false, 写入0，代表显示虚拟键
		 */
		private void hideNaviBar(boolean hide) {
			ContentValues values = new ContentValues();
			values.put("name", NAVI_KEY_HIDE);
			values.put("value", (hide ? 1 : 0));
			ContentResolver cr = mContext.getContentResolver();
			cr.insert(Settings.System.CONTENT_URI, values);
		}

		private void setNavBarColor(int mode, boolean putColor) {
			Intent intent = new Intent(NAV_ACTION);
			intent.putExtra("mode", mode);
			if (putColor) {
				intent.putExtra("color", NAV_COLOR);
			}
			mContext.sendBroadcast(intent);
		}


		private void initAnim() {
			
			airModeIconAnim = new FrameAnimation(mContext,
					FrameAnimation.ICON_AIRMODE_DOWN);
			rebootIconAnim = new FrameAnimation(mContext,
					FrameAnimation.ICON_REBOOT_DOWN);
			shutdownIconAnim = new FrameAnimation(mContext,
					FrameAnimation.ICON_SHUT_DOWN);
			
			airModeIconAnim.setView(airmode);
			rebootIconAnim.setView(reboot);
			shutdownIconAnim.setView(shutdown);
			
			rebootAnim = AnimationUtils.loadAnimation(mContext,
					com.aurora.R.anim.aurora_airmode_anim);
			airModeAnim = AnimationUtils.loadAnimation(mContext,
					com.aurora.R.anim.aurora_airmode_anim);
			shutdownAnim = AnimationUtils.loadAnimation(mContext,
					com.aurora.R.anim.aurora_airmode_anim);
			mDismissAirMode = AnimationUtils.loadAnimation(mContext,
					com.aurora.R.anim.aurora_shutdown_dialog_push_up_out);
			mDismissAnim = AnimationUtils.loadAnimation(mContext,
					com.aurora.R.anim.aurora_shutdown_dialog_push_up_out);
			mDismissReboot = AnimationUtils.loadAnimation(mContext,
					com.aurora.R.anim.aurora_shutdown_dialog_push_up_out);

			
			mAirmodeTextDismissAnim = AnimationUtils.loadAnimation(mContext,
					com.aurora.R.anim.aurora_shutdown_dialog_push_up_out);
			mShutdownTextDismissAnim = AnimationUtils.loadAnimation(mContext,
					com.aurora.R.anim.aurora_shutdown_dialog_push_up_out);
			mRebootTextDismissAnim = AnimationUtils.loadAnimation(mContext,
					com.aurora.R.anim.aurora_shutdown_dialog_push_up_out);
			
			
			mAirmodeTextAnim = AnimationUtils.loadAnimation(mContext,
					com.aurora.R.anim.aurora_shutdown_text_anim);

			mShutdownTextAnim=AnimationUtils.loadAnimation(mContext,
					com.aurora.R.anim.aurora_shutdown_text_anim);
			mRebootTextAnim=AnimationUtils.loadAnimation(mContext,
					com.aurora.R.anim.aurora_shutdown_text_anim);
			
			mAirmodeTextDismissAnim.setAnimationListener(this);
			mShutdownTextDismissAnim.setAnimationListener(this);
			mRebootTextDismissAnim.setAnimationListener(this);
			
			mAirmodeTextAnim.setAnimationListener(this);
			mShutdownTextAnim.setAnimationListener(this);
			mRebootTextAnim.setAnimationListener(this);
			
			mDismissAirMode.setAnimationListener(this);
			mDismissAnim.setAnimationListener(this);
			airModeIconAnim.setAnimationImageListener(this);
			rebootIconAnim.setAnimationImageListener(this);
			shutdownIconAnim.setAnimationImageListener(this);
			rebootAnim.setAnimationListener(this);
			airModeAnim.setAnimationListener(this);
			mDismissReboot.setAnimationListener(this);
		}

		private void playAnimation() {
			airmode.startAnimation(airModeAnim);
			layoutAirMode.setVisibility(View.VISIBLE);
			mAnimHandler.sendEmptyMessageDelayed(MSG_PLAY_TEXT_ANIM, 300);
		}

		private void initRebootConfirmAlert() {
			AlertDialog.Builder alert = new AlertDialog.Builder(mContext);
			alert.setTitle(com.aurora.R.string.reboot_alert_title);
			alert.setMessage(com.aurora.R.string.reboot_alert_message);
			alert.setPositiveButton(com.aurora.R.string.reboot_ok,
					new DialogInterface.OnClickListener() {

						@Override
						public void onClick(DialogInterface dialog, int which) {
							// TODO Auto-generated method stub
							Intent i = new Intent(Intent.ACTION_REBOOT);
							i.putExtra("nowait", 1);
							i.putExtra("interval", 1);
							i.putExtra("window", 0);
							mContext.sendBroadcast(i);
							dialog.dismiss();
							dismissDialog();
						}
					});
			alert.setNegativeButton(com.aurora.R.string.reboot_cancel,
					new DialogInterface.OnClickListener() {

						@Override
						public void onClick(DialogInterface dialog, int which) {
							// TODO Auto-generated method stub
							dialog.dismiss();

						}
					});

			mRebootConfirmAlert = alert.create();

			mRebootConfirmAlert.getWindow().setType(
					WindowManager.LayoutParams.TYPE_KEYGUARD_DIALOG);
		}

		private void dismissDialog() {
			playDismissAnim();
		}

		@Override
		protected void onStart() {
			// If global accessibility gesture can be performed, we will take
			// care
			// of dismissing the dialog on touch outside. This is because the
			// dialog
			// is dismissed on the first down while the global gesture is a long
			// press
			// with two fingers anywhere on the screen.
			if (EnableAccessibilityController
					.canEnableAccessibilityViaGesture(mContext)) {
				mEnableAccessibilityController = new EnableAccessibilityController(
						mContext);
				super.setCanceledOnTouchOutside(false);
			} else {
				mEnableAccessibilityController = null;
				super.setCanceledOnTouchOutside(true);
			}
			super.onStart();
		}

		@Override
		protected void onStop() {
			if (mEnableAccessibilityController != null) {
				mEnableAccessibilityController.onDestroy();
			}
			setNavBarColor(NAV_MODE_CANCLE, false);
			super.onStop();
		}

		private boolean isKeyGuardShowing() {
			KeyguardManager mKeyguardManager = (KeyguardManager) mContext
					.getSystemService(Context.KEYGUARD_SERVICE);
			return mKeyguardManager.inKeyguardRestrictedInputMode();
		}

		@Override
		public boolean dispatchTouchEvent(MotionEvent event) {
			if (mEnableAccessibilityController != null) {
				final int action = event.getActionMasked();
				if (action == MotionEvent.ACTION_DOWN) {
					View decor = getWindow().getDecorView();
					final int eventX = (int) event.getX();
					final int eventY = (int) event.getY();
					if (eventX < -mWindowTouchSlop
							|| eventY < -mWindowTouchSlop
							|| eventX >= decor.getWidth() + mWindowTouchSlop
							|| eventY >= decor.getHeight() + mWindowTouchSlop) {
						mCancelOnUp = true;
					}
				}
				try {
					if (!mIntercepted) {
						mIntercepted = mEnableAccessibilityController
								.onInterceptTouchEvent(event);
						if (mIntercepted) {
							final long now = SystemClock.uptimeMillis();
							event = MotionEvent.obtain(now, now,
									MotionEvent.ACTION_CANCEL, 0.0f, 0.0f, 0);
							event.setSource(InputDevice.SOURCE_TOUCHSCREEN);
							mCancelOnUp = true;
						}
					} else {
						return mEnableAccessibilityController
								.onTouchEvent(event);
					}
				} finally {
					if (action == MotionEvent.ACTION_UP) {
						if (mCancelOnUp) {
							cancel();
						}
						mCancelOnUp = false;
						mIntercepted = false;
					}
				}
			}
			return super.dispatchTouchEvent(event);
		}

		@Override
		public boolean onKeyDown(int keyCode, KeyEvent event) {
			// TODO Auto-generated method stub
			if (keyCode == KeyEvent.KEYCODE_BACK) {
				if (mKeyBack == 0) {
					dismissDialog();
					mKeyBack = 1;
				}

				return true;
			}
			return super.onKeyDown(keyCode, event);
		}

		@Override
		public void onWindowFocusChanged(boolean hasFocus) {
			// TODO Auto-generated method stub
			if (hasFocus) {
				setStatusBarBG(true);
			}
			super.onWindowFocusChanged(hasFocus);

		}

		public void setStatusBarBG(boolean isTransparent) {
			Intent StatusBarBGIntent = new Intent();
			StatusBarBGIntent
					.setAction("aurora.action.SET_STATUSBAR_TRANSPARENT");
			StatusBarBGIntent.putExtra("transparent", isTransparent);
			mContext.sendBroadcast(StatusBarBGIntent);
		}

		private void setStatusBarTransparent(boolean enable) {
			Log.i("aaa", "setStatusBarTransparent:" + enable);
			NotificationManager notificationManager = (NotificationManager) mContext
					.getSystemService(Context.NOTIFICATION_SERVICE);
			Notification.Builder builder = new Builder(mContext);
			builder.setSmallIcon(com.aurora.R.drawable.stat_sys_secure);
			String tag = "auroraSBNT653";
			if (enable) {
				tag = "auroraSBT8345";
			} else {
				tag = "auroraSBNT653";
			}
			notificationManager.notify(tag, 0, builder.build());
		}

		public void playSounds(int sound, int number) {
			AudioManager am = (AudioManager) mContext.getSystemService(Context.AUDIO_SERVICE);
			int ringtongType = am.getRingerMode();
			if(ringtongType == AudioManager.RINGER_MODE_SILENT
					||ringtongType == AudioManager.RINGER_MODE_VIBRATE){
				return;
			}
			MediaPlayer mediaPlayer = new MediaPlayer();
//			mediaPlayer.setAudioStreamType(AudioManager.STREAM_SYSTEM);
			mediaPlayer.setOnCompletionListener(new OnCompletionListener() {

				@Override
				public void onCompletion(MediaPlayer mp) {
					// TODO Auto-generated method stub
					if (mp != null) {
						mp.stop();
						mp.release();
					}
				}
			});
			try {
				mediaPlayer.setDataSource(mSoundPath);
				mediaPlayer.prepare();
				mediaPlayer.setLooping(false);
				mediaPlayer.start();
			} catch (Exception e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}

		}

		private void playDismissAnim() {
			 airmode.startAnimation(mDismissAirMode);
			 textAirMode.startAnimation(mAirmodeTextDismissAnim);
//	         layoutAirMode.setVisibility(View.INVISIBLE);
	         mAnimHandler.sendEmptyMessageDelayed(MSG_PLAY_REBOOT_DISMISS,20);
		}

		@Override
		public void show() {
			super.show();
			endTime =System.currentTimeMillis();
			Log.e("time", "show Time:"+(endTime - startTime));
			playSounds(1, 1);
			playAnimation();
			/*
			 * if(mNavView != null){ if(isKeyGuardShowing()){
			 * mNavView.setVisibility(View.VISIBLE); } }else{
			 * mNavView.setVisibility(View.GONE); }
			 */
			mContext.sendBroadcast(new Intent(
					"com.aurora.action.SHUTDOWN_DIALOG_SHOW"));
			setNavBarColor(NAV_MODE_SET_COLOR, true);
		}

		@Override
		public void onClick(View v) {

			switch (v.getId()) {
			case com.aurora.R.id.aurora_shutdown_btn:
				mWindowManagerFuncs.shutdown(true);
				dismissDialog();
				break;
			case com.aurora.R.id.aurora_reboot_btn:
				mRebootConfirmAlert.show();
				dismissDialog();
				break;
			case com.aurora.R.id.aurora_airmode_btn:
				isAirMode = !isAirMode;
				if (mHasTelephony
						&& Boolean.parseBoolean(SystemProperties
								.get(TelephonyProperties.PROPERTY_INECM_MODE))) {
					mIsWaitingForEcmExit = true;
					// Launch ECM exit dialog
					Intent ecmDialogIntent = new Intent(
							TelephonyIntents.ACTION_SHOW_NOTICE_ECM_BLOCK_OTHERS,
							null);
					ecmDialogIntent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
					mContext.startActivity(ecmDialogIntent);
				} else {
					changeAirplaneModeSystemSetting(isAirMode);
				}

				// In ECM mode airplane state cannot be changed
				mAirplaneState = isAirMode ? ToggleAction.State.TurningOn
						: ToggleAction.State.TurningOff;
				textAirMode.setText(isAirMode ? airmodeClose : airmodeOpen);
				dismissDialog();
				break;

			default:
				break;
			}

		}

		private void ActivityInitFunction() {
			mSettingsObserver = new SettingsObserver(mNavigationKeyHandler);
			mSettingsObserver.observe();
		}

		@Override
		public boolean onLongClick(View v) {
			mWindowManagerFuncs.rebootSafeMode(true);
			dismiss();
			return true;
		}

		class SettingsObserver extends ContentObserver {
			SettingsObserver(Handler handler) {
				super(handler);
			}

			void observe() {
				ContentResolver resolver = mContext.getContentResolver();
				resolver.registerContentObserver(
						Settings.System.getUriFor(NAVI_KEY_HIDE), false, this);
			}

			@Override
			public void onChange(boolean selfChange) {
				if (selfChange) {

				}
				update();
			}

			void update() {
				// your logic code
			}
		}

		@Override
		public void onAnimationEnd(Animation animation) {
          //			 TODO Auto-generated method stub
		if(animation == mDismissAnim){
				dismiss();
			}
		if(animation == mRebootTextAnim){
			textAirMode.setVisibility(View.VISIBLE);
			textReboot.setVisibility(View.VISIBLE);
			textShutdown.setVisibility(View.VISIBLE);
		}
		
		if(animation == mAirmodeTextDismissAnim){
			layoutAirMode.setVisibility(View.INVISIBLE);
		}
		if(animation == mRebootTextDismissAnim){
			  layoutReboot.setVisibility(View.INVISIBLE);
		}
		if(animation == mShutdownTextDismissAnim){
			  layoutShutdown.setVisibility(View.INVISIBLE);
		}
		}

		@Override
		public void onAnimationRepeat(Animation animation) {
			// TODO Auto-generated method stub
			
		}

		@Override
		public void onAnimationStart(Animation animation) {
			// TODO Auto-generated method stub
			if (animation == airModeAnim) {
				mAnimHandler.sendEmptyMessage(MSG_PLAY_REBOOT_ANIM);
				mAnimHandler.sendEmptyMessageDelayed(MSG_PLAY_ICON_ANIM, 400);
			}else if(animation == rebootAnim){
				mAnimHandler.sendEmptyMessage(MSG_PLAY_SHUTDOWN_ANIM);
			}else if(animation == mDismissReboot){
				mAnimHandler.sendEmptyMessageDelayed(MSG_PLAY_SHUTDOWN_DISMISS,20);
				
			}
		}

		@Override
		public void onAnimationStart(FrameAnimation animation) {
		}

		@Override
		public void onAnimationEnd(FrameAnimation animation) {
			// TODO Auto-generated method stub
			if(animation == airModeIconAnim){
				airmode.setBackgroundResource(com.aurora.R.drawable.aurora_airmode_selector);
			}else if(animation == rebootIconAnim){
				reboot.setBackgroundResource(com.aurora.R.drawable.aurora_reboot_selector);
			}else if(animation == shutdownIconAnim){
				shutdown.setBackgroundResource(com.aurora.R.drawable.aurora_shutdown_selector);
			}
		}

		@Override
		public void onRepeat(int repeatIndex) {
		}

		@Override
		public void onFrameChange(int repeatIndex, int frameIndex,
				int currentTime) {
		}
	}    
    
    class FrameAnimation {

    	public static final int ICON_SHUT_DOWN = 1;
    	public static final int ICON_AIRMODE_DOWN = 2;
    	public static final int ICON_REBOOT_DOWN = 3;

    	public int mIconType;


    	private Handler handler;
    	private View view;
    	private AnimationImageListener animationImageListener;

    	private FrameCallback[] callbacks;
    	private Drawable[] frames;
    	private Drawable[] frames2;
    	private ArrayList<Integer> mDrawables;
    	private int[] durations;
    	private int frameCount;

    	private boolean isRun;
    	private boolean fillAfter;
    	private boolean isOneShot;
    	private boolean isLimitless;
    	private int repeatTime;

    	private int currentRepeat;
    	private int currentFrame;
    	private int currentTime;
    	private int mLoadDivider;
    	private AnimationDrawable mAnimDrawable;
    	private Context mContext;

    	private Runnable nextFrameRun = new Runnable() {
    		public void run() {
    			if (!isRun) {
    				end();
    				return;
    			}
    			currentTime += durations[currentFrame];
    			if (callbacks[currentFrame] != null) {
    				callbacks[currentFrame].onFrameEnd(currentFrame);
    			}
    			nextFrame();
    		}
    	};

    	public FrameAnimation(Context context,  int iconType) {
    		mContext = context;
    		int animRes = 0;
    		switch (iconType) {
    		case ICON_SHUT_DOWN:
    			animRes = com.aurora.R.anim.aurora_shutdown_icon;
    			break;
    		case ICON_REBOOT_DOWN:
    			animRes = com.aurora.R.anim.aurora_reboot_icon;
    			break;
    		case ICON_AIRMODE_DOWN:
    			animRes = com.aurora.R.anim.aurora_airmode_icon;
    			break;

    		default:
    			break;
    		}
    		mAnimDrawable =(AnimationDrawable) mContext.getResources()
    				.getDrawable(animRes);
    		this.handler = new Handler();
    			init();
    	}
    	public void setView(View view){
    		this.view = view;
    	}

    	private void init() {
    		this.frameCount = mAnimDrawable.getNumberOfFrames();
    		this.frames = new Drawable[frameCount];
    		this.frames2 = new Drawable[frameCount];
    		this.callbacks = new FrameCallback[frameCount];
    		this.isRun = false;
    		this.fillAfter = false;
    		this.isOneShot = true;
    		this.isLimitless = false;
    		this.repeatTime = 2;
    		durations = new int[frameCount];
    		long startTime = System.currentTimeMillis();
    		for (int i = 0; i < frameCount; i++) {
    			frames[i] = mAnimDrawable.getFrame(i);
    			durations[i] = 10;
    		}
    	}

    	public void start() {
    		if (isRun) {
    			return;
    		}
    		this.isRun = true;
    		this.currentRepeat = -1;
    		this.currentFrame = -1;
    		this.currentTime = 0;
    		if (animationImageListener != null) {
    			animationImageListener.onAnimationStart(this);
    		}
    		startProcess();
    	}

    	public void stop() {
    		this.isRun = false;
    	}

    	private void startProcess() {
    		this.currentFrame = -1;
    		this.currentTime = 0;
    		this.currentRepeat++;
    		if (animationImageListener != null) {
    			animationImageListener.onRepeat(currentRepeat);
    		}
    		nextFrame();
    	}

    	private void endProcess() {
    		if (isOneShot || (!isLimitless && currentRepeat >= repeatTime - 1)
    				|| !isRun) {
    			end();
    		} else {
    			startProcess();
    		}
    	}

    	private void end() {
    		if (!fillAfter && frameCount > 0) {
    			view.setBackgroundDrawable(frames[0]);
    		}
    		if (animationImageListener != null) {
    			animationImageListener.onAnimationEnd(this);
    		}
    		this.isRun = false;
    	}

    	private void nextFrame() {
    		if (currentFrame == frameCount - 1) {
    			endProcess();
    			return;
    		}

    		currentFrame++;

    		changeFrame(currentFrame);

    		handler.postDelayed(nextFrameRun, durations[currentFrame]);
    	}

    	private void changeFrame(int frameIndex) {
    		synchronized (frames) {
    			view.setBackgroundDrawable(frames[frameIndex]);
    			frames[frameIndex] = null;
    			if (animationImageListener != null) {
    				animationImageListener.onFrameChange(currentRepeat, frameIndex,
    						currentTime);
    			}
    			if (callbacks[currentFrame] != null) {
    				callbacks[currentFrame].onFrameStart(frameIndex);
    			}

    		}
    	}

    	public int getSumDuration() {
    		int sumDuration = 0;
    		for (int duration : durations) {
    			sumDuration += duration;
    		}
    		return sumDuration;
    	}

    	public boolean isOneShot() {
    		return isOneShot;
    	}

    	public void setOneShot(boolean isOneShot) {
    		this.isOneShot = isOneShot;
    	}

    	public boolean isFillAfter() {
    		return fillAfter;
    	}

    	public void setFillAfter(boolean fillAfter) {
    		this.fillAfter = fillAfter;
    	}

    	public boolean isLimitless() {
    		return isLimitless;
    	}

    	public void setLimitless(boolean isLimitless) {
    		if (isLimitless) {
    			setOneShot(false);
    		}
    		this.isLimitless = isLimitless;
    	}

    	public void addFrameCallback(int index, FrameCallback callback) {
    		this.callbacks[index] = callback;
    	}

    	public void setAnimationImageListener(
    			AnimationImageListener animationImageListener) {
    		this.animationImageListener = animationImageListener;
    	}

    	public int getRepeatTime() {
    		return repeatTime;
    	}

    	public void setRepeatTime(int repeatTime) {
    		this.repeatTime = repeatTime;
    	}



    }
	public interface AnimationImageListener {
		public void onAnimationStart(FrameAnimation animation);

		public void onAnimationEnd(FrameAnimation animation);

		public void onRepeat(int repeatIndex);

		public void onFrameChange(int repeatIndex, int frameIndex,
				int currentTime);
	}

	public interface FrameCallback {
		public void onFrameStart(int startTime);

		public void onFrameEnd(int endTime);
	}
}