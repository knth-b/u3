/*
 * Copyright (C) 2009 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.android.internal.policy.impl;
import android.content.Context;
import android.os.FileUtils;
import android.util.Log;
import java.util.ArrayList;
import java.util.List;
import android.app.ActivityManager;
import android.app.ActivityManager.RunningAppProcessInfo;

public class AmigoMmiUtil {

	private final static String MMI_PROCESS_NAME = "gn.com.android.mmitest";
	private final static String ATUO_MMI_PROCESS_NAME = "com.gionee.autommi";

	public  static boolean isMMITestRunning( Context mContext) {
		if(mContext == null){
           return false;
		}
        boolean isRunning = false;
        ActivityManager _ActivityManager = (ActivityManager) mContext
                .getSystemService(Context.ACTIVITY_SERVICE);
        List<RunningAppProcessInfo> list = _ActivityManager.getRunningAppProcesses();
        if(list != null) {
            for (int j = 0; j < list.size(); j++) {
                if ((MMI_PROCESS_NAME.equals(list.get(j).processName)) ||(ATUO_MMI_PROCESS_NAME.equals(list.get(j).processName))) {
                    isRunning = true;
                    break;
                }
            }
        }
        return isRunning;
    }	
}
