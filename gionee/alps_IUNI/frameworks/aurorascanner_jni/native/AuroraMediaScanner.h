/*
******************************************************
*	深圳市福田区深南大道7888东海国际中心B座16层IUNI科技		
*	文 件 名：AuroraMediaScanner.h
*	创建时间：2014-04-14
*	文件描述：多媒体扫描文件底层处理
*====================================================
*	维护人：hjw
*====================================================
* 	代码版本：1.0
*	修改时间：2014-04-14
*	修改人：hjw
*	修改描述：创建
*====================================================
*	遗留问题：
*
******************************************************
*/
#ifndef INUISCANNER_H_
#define INUISCANNER_H_

#include <utils/Log.h>
#include <utils/threads.h>
#include <utils/List.h>
#include <utils/Errors.h>
#include <pthread.h>


//文件节点结构体，可返回文件或者文件夹类型的节点
typedef struct dirent IUNI_FILE_DIRENT;


//android域
namespace android {
	
class AuroraMediaScannerClient;
class StringArray;
enum MediaScanResult {
    
    //扫描任务成功
    MEDIA_SCAN_RESULT_OK,
    
    //可以略过的文件扫描对象
    MEDIA_SCAN_RESULT_SKIPPED,
    
    //扫描错误
    MEDIA_SCAN_RESULT_ERROR,
};




struct AuroraMediaScanner {
    AuroraMediaScanner();
    virtual ~AuroraMediaScanner();


    virtual MediaScanResult processFile(
            const char *path, const char *mimeType, AuroraMediaScannerClient &client) = 0;

    //扫描目录
    virtual MediaScanResult processDirectory(
            const char *path, AuroraMediaScannerClient &client);


    void setLocale(const char *locale);
protected:
    const char *locale() const;
private:
    char *mLocale;
    char *mSkipList;
    int *mSkipIndex;
    

    MediaScanResult doProcessDirectory(
            char *path, int pathRemaining, AuroraMediaScannerClient &client, bool noMedia);
    MediaScanResult doProcessDirectoryEntry(
            char *path, int pathRemaining, AuroraMediaScannerClient &client, bool noMedia,
            struct dirent* entry, char* fileSpot);

    void loadSkipList();
    bool shouldSkipDirectory(char *path);


    AuroraMediaScanner(const AuroraMediaScanner &);
    AuroraMediaScanner &operator=(const AuroraMediaScanner &);    
};

class AuroraMediaScannerClient
{
public:
    AuroraMediaScannerClient();
    virtual ~AuroraMediaScannerClient();
    void setLocale(const char* locale);
    void beginFile();
    status_t addStringTag(const char* name, const char* value);
    void endFile();

    virtual status_t scanFile(const char* path, long long lastModified,
            long long fileSize, bool isDirectory, bool noMedia) = 0;
    virtual status_t handleStringTag(const char* name, const char* value) = 0;
    virtual status_t setMimeType(const char* mimeType) = 0;

protected:
    void convertValues(uint32_t encoding);

protected:
    // cached name and value strings, for native encoding support.
    StringArray*    mNames;
    StringArray*    mValues;

    // default encoding based on MediaScanner::mLocale string
    uint32_t        mLocaleEncoding;
};



}






#endif/*INUISCANNER_H_*/
