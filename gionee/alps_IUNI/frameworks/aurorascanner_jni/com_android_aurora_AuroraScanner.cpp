#include <jni.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "JNIHelp.h"
#include "android_runtime/AndroidRuntime.h"


#include "native/AuroraMetaMediaScanner.h"

using namespace android;

struct fields_t {
    jfieldID    context;
};

static fields_t fields;



static const char* const kClassAuroracannerClient =
        "com/android/aurora/AuroraMediaScannerClient";

static const char* const kClassAuroraScanner =
        "com/android/aurora/AuroraMediaScanner";

static const char* const kRunTimeException =
        "java/lang/RuntimeException";

static const char* const kIllegalArgumentException =
        "java/lang/IllegalArgumentException";

static status_t checkAndClearExceptionFromCallback(JNIEnv* env, const char* methodName) {
    if (env->ExceptionCheck()) {
        ALOGV("An exception was thrown by callback '%s'.", methodName);
        //LOGE_EX(env);
        env->ExceptionClear();
        return UNKNOWN_ERROR;
    }
    return OK;
}

static bool isValidUtf8(const char* bytes) {
    while (*bytes != '\0') {
        unsigned char utf8 = *(bytes++);
        // Switch on the high four bits.
        switch (utf8 >> 4) {
        case 0x00:
        case 0x01:
        case 0x02:
        case 0x03:
        case 0x04:
        case 0x05:
        case 0x06:
        case 0x07:
            // Bit pattern 0xxx. No need for any extra bytes.
            break;
        case 0x08:
        case 0x09:
        case 0x0a:
        case 0x0b:
        case 0x0f:
            /*
             * Bit pattern 10xx or 1111, which are illegal start bytes.
             * Note: 1111 is valid for normal UTF-8, but not the
             * modified UTF-8 used here.
             */
            return false;
        case 0x0e:
            // Bit pattern 1110, so there are two additional bytes.
            utf8 = *(bytes++);
            if ((utf8 & 0xc0) != 0x80) {
                return false;
            }
            // Fall through to take care of the final byte.
        case 0x0c:
        case 0x0d:
            // Bit pattern 110x, so there is one additional byte.
            utf8 = *(bytes++);
            if ((utf8 & 0xc0) != 0x80) {
                return false;
            }
            break;
        } 
    }
    return true;
}

class MyAuroraMediaScannerClient :public AuroraMediaScannerClient
{
public:
  MyAuroraMediaScannerClient(JNIEnv *env, jobject client)
        :   mEnv(env),
            mClient(env->NewGlobalRef(client)),
            mScanFileMethodID(0),
            mHandleStringTagMethodID(0),
            mSetMimeTypeMethodID(0)
    {
        ALOGV("MyAuroraMediaScannerClient constructor");
        jclass mediaScannerClientInterface =
                env->FindClass(kClassAuroracannerClient);

        if (mediaScannerClientInterface == NULL) {
            ALOGV("Class %s not found", kClassAuroracannerClient);
        } else {
            mScanFileMethodID = env->GetMethodID(
                                    mediaScannerClientInterface,
                                    "auroraScanFile",
                                    "(Ljava/lang/String;JJZZ)V");

            mHandleStringTagMethodID = env->GetMethodID(
                                    mediaScannerClientInterface,
                                    "auroraHandleStringTag",
                                    "(Ljava/lang/String;Ljava/lang/String;)V");

            mSetMimeTypeMethodID = env->GetMethodID(
                                    mediaScannerClientInterface,
                                    "auroraSetMimeType",
                                    "(Ljava/lang/String;)V");
        }
    }

    virtual ~MyAuroraMediaScannerClient()
    {
        ALOGV("MyAuroraMediaScannerClient destructor");
        mEnv->DeleteGlobalRef(mClient);
    }

    virtual status_t scanFile(const char* path, long long lastModified,
            long long fileSize, bool isDirectory, bool noMedia)
    {
        ALOGV("scanFile: path(%s), time(%lld), size(%lld) and isDir(%d)",
            path, lastModified, fileSize, isDirectory);

        jstring pathStr;
        if ((pathStr = mEnv->NewStringUTF(path)) == NULL) {
            mEnv->ExceptionClear();
            return NO_MEMORY;
        }

        mEnv->CallVoidMethod(mClient, mScanFileMethodID, pathStr, lastModified,
                fileSize, isDirectory, noMedia);

        mEnv->DeleteLocalRef(pathStr);
        return checkAndClearExceptionFromCallback(mEnv, "scanFile");
    }

    //把处理的数据返回到java
    virtual status_t handleStringTag(const char* name, const char* value)
    {
        ALOGV("handleStringTag: name(%s) and value(%s)", name, value);
        jstring nameStr, valueStr;
        if ((nameStr = mEnv->NewStringUTF(name)) == NULL) {
            mEnv->ExceptionClear();
            return NO_MEMORY;
        }
        char *cleaned = NULL;
        if (!isValidUtf8(value)) {
            cleaned = strdup(value);
            char *chp = cleaned;
            char ch;
            while ((ch = *chp)) {
                if (ch & 0x80) {
                    *chp = '?';
                }
                chp++;
            }
            value = cleaned;
        }
        valueStr = mEnv->NewStringUTF(value);
        free(cleaned);
        if (valueStr == NULL) {
            mEnv->DeleteLocalRef(nameStr);
            mEnv->ExceptionClear();
            return NO_MEMORY;
        }

        mEnv->CallVoidMethod(
            mClient, mHandleStringTagMethodID, nameStr, valueStr);

        mEnv->DeleteLocalRef(nameStr);
        mEnv->DeleteLocalRef(valueStr);
        return checkAndClearExceptionFromCallback(mEnv, "handleStringTag");
    }

    virtual status_t setMimeType(const char* mimeType)
    {
        ALOGV("setMimeType: %s", mimeType);
        jstring mimeTypeStr;
        if ((mimeTypeStr = mEnv->NewStringUTF(mimeType)) == NULL) {
            mEnv->ExceptionClear();
            return NO_MEMORY;
        }

        mEnv->CallVoidMethod(mClient, mSetMimeTypeMethodID, mimeTypeStr);

        mEnv->DeleteLocalRef(mimeTypeStr);
        return checkAndClearExceptionFromCallback(mEnv, "setMimeType");
    }

private:
    JNIEnv *mEnv;
    jobject mClient;
    jmethodID mScanFileMethodID;
    jmethodID mHandleStringTagMethodID;
    jmethodID mSetMimeTypeMethodID;
};

static AuroraMediaScanner *getNativeScanner_l(JNIEnv* env, jobject thiz)
{
    return (AuroraMediaScanner *) env->GetIntField(thiz, fields.context);
}

static void setNativeScanner_l(JNIEnv* env, jobject thiz, AuroraMediaScanner *s)
{
    env->SetIntField(thiz, fields.context, (int)s);
}




//多媒体扫描目录
static void
com_android_aurora_processDirectory(
        JNIEnv *env, jobject thiz, jstring path, jobject client)
{
    ALOGV("com_android_aurora_processDirectory");


    AuroraMediaScanner *mp = getNativeScanner_l(env, thiz);
    if (mp == NULL) {
        jniThrowException(env, kRunTimeException, "No scanner available");
        return;
    }

    if (path == NULL) {
        jniThrowException(env, kIllegalArgumentException, NULL);
        return;
    }

    const char *pathStr = env->GetStringUTFChars(path, NULL);
    if (pathStr == NULL) {  
        return;
    }

    MyAuroraMediaScannerClient myClient(env, client);

  
    MediaScanResult result = mp->processDirectory(pathStr, myClient);
    if (result == MEDIA_SCAN_RESULT_ERROR) {
        ALOGV("An error occurred while scandning directory '%s'.", pathStr);
    }
    env->ReleaseStringUTFChars(path, pathStr);
}

//扫描单个文件
static void
com_android_aurora_processFile(
        JNIEnv *env, jobject thiz, jstring path,
        jstring mimeType, jobject client)
{
    ALOGV("processFile");

    
    AuroraMediaScanner *mp = getNativeScanner_l(env, thiz);
    if (mp == NULL) {
        jniThrowException(env, kRunTimeException, "No scanner available");
        return;
    }

    if (path == NULL) {
        jniThrowException(env, kIllegalArgumentException, NULL);
        return;
    }

    const char *pathStr = env->GetStringUTFChars(path, NULL);
    if (pathStr == NULL) {  
        return;
    }

    const char *mimeTypeStr =
        (mimeType ? env->GetStringUTFChars(mimeType, NULL) : NULL);
    if (mimeType && mimeTypeStr == NULL) {  // Out of memory
        // ReleaseStringUTFChars can be called with an exception pending.
        env->ReleaseStringUTFChars(path, pathStr);
        return;
    }

    MyAuroraMediaScannerClient myClient(env, client);
    MediaScanResult result = mp->processFile(pathStr, mimeTypeStr, myClient);
    if (result == MEDIA_SCAN_RESULT_ERROR) {
        ALOGE("An error occurred while scanning file '%s'.", pathStr);
    }
    env->ReleaseStringUTFChars(path, pathStr);
    if (mimeType) {
        env->ReleaseStringUTFChars(mimeType, mimeTypeStr);
    }
}

static void
com_android_aurora_setLocale(
        JNIEnv *env, jobject thiz, jstring locale)
{
    ALOGV("setLocale");
    AuroraMediaScanner *mp = getNativeScanner_l(env, thiz);
    if (mp == NULL) {
        jniThrowException(env, kRunTimeException, "No scanner available");
        return;
    }

    if (locale == NULL) {
       jniThrowException(env, kIllegalArgumentException, NULL);
        return;
    }
    const char *localeStr = env->GetStringUTFChars(locale, NULL);
    if (localeStr == NULL) {  
        return;
    }
    mp->setLocale(localeStr);

    env->ReleaseStringUTFChars(locale, localeStr);
}

//多媒体扫描JNI环境初始化
static void
com_android_aurora_native_init (JNIEnv *env)
{
	ALOGV("com_android_aurora_native_init  destructor");
	jclass clazz = env->FindClass(kClassAuroraScanner);
	if (clazz == NULL) {
		ALOGV("ERROR: FindClass AuroraMediaScanner\n");
		return;
	}

	fields.context = env->GetFieldID(clazz, "mNativeContext", "I");
	if (fields.context == NULL) {
		ALOGV("ERROR: GetFieldID mNativeContext\n");
		return;
	}
}


//多媒体扫描JNI环境设置
static void
com_android_aurora_native_setup(JNIEnv *env, jobject thiz)
{
    ALOGV("com_android_aurora_native_setup");

    //把Scanner放置到Field中
    AuroraMediaScanner *mp = new AuroraMetaMediaScanner;

    if (mp == NULL) {
        jniThrowException(env, kRunTimeException, "Out of memory");
        return;
    }

    env->SetIntField(thiz, fields.context, (int)mp);
}

//多媒体扫描JNI环境清空
static void
com_android_aurora_native_finalize(JNIEnv *env, jobject thiz)
{
    ALOGV("native_finalize");
    AuroraMediaScanner *mp = getNativeScanner_l(env, thiz);
    if (mp == 0) {
        return;
    }
    delete mp;
    setNativeScanner_l(env, thiz, 0);
}

static JNINativeMethod gMethods[] =
{
	{
		"auroraProgressDirectory",
		"(Ljava/lang/String;Lcom/android/aurora/AuroraMediaScannerClient;)V",
		(void *)com_android_aurora_processDirectory
	},
	{
		"auroraProgressFile",
		"(Ljava/lang/String;Ljava/lang/String;Lcom/android/aurora/AuroraMediaScannerClient;)V",
		(void *)com_android_aurora_processFile
	},
	{
		"auroraSetLocale",
		"(Ljava/lang/String;)V",
		(void *)com_android_aurora_setLocale
	},
	{
		"auroraNativeInit", 
		"()V",
		(void*) com_android_aurora_native_init  
	},
	{
		"auroraNativeSetup",
		"()V",
		(void *)com_android_aurora_native_setup
	},
	{
		"auroraNativeFinalize",
		"()V",
		(void *)com_android_aurora_native_finalize
	},
};

//注册JNI方法
int register_os_android_AuroraScanenr(JNIEnv* env)
{
     return AndroidRuntime::registerNativeMethods(env,
            kClassAuroraScanner, gMethods, NELEM(gMethods));
}

jint JNI_OnLoad(JavaVM* vm, void* reserved)
{
	JNIEnv* env = NULL;
	jint result = -1;
	jclass native = NULL;

	if (vm->GetEnv((void**) &env, JNI_VERSION_1_4) != JNI_OK)
	{
		return -1;
	}

	result = register_os_android_AuroraScanenr(env);
	if (result < 0)
	{
		return -1;
	}
	return JNI_VERSION_1_4;
}
