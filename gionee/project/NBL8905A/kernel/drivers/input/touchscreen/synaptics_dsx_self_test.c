/*
 * Synaptics DSX touchscreen driver
 *
 * Copyright (C) 2012 Synaptics Incorporated
 *
 * Copyright (C) 2012 Alexandra Chin <alexandra.chin@tw.synaptics.com>
 * Copyright (C) 2012 Scott Lin <scott.lin@tw.synaptics.com>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 */
#include <linux/kernel.h>
#include <linux/module.h>
#include <linux/slab.h>
#include <linux/i2c.h>
#include <linux/interrupt.h>
#include <linux/delay.h>
#include <linux/input.h>
#include <linux/ctype.h>
#include <linux/hrtimer.h>
#include "gn_synaptics_rmi4_ts.h"
#include "synaptics_dsx_self_test.h"
#include <linux/input/gn_synaptics_dsx.h>

/*
#define RAW_HEX
#define HUMAN_READABLE
*/



#define CFG_F54_TXCOUNT 13
#define CFG_F54_RXCOUNT 23

#define F54_HR_RX_OFFSET 620
#define F54_HR_TX_OFFSET 620
#define F54_HR_MIN       -400

//#define RAW_DATA_MAX 3700
//#define RAW_DATA_MIN 2276



static short UpperLimit[13][23] ={	
{3603,3587,3715,3731,3759,3783,3768,3797,3813,3835,3873,3535,3537,3537,3543,3547,3550,3543,3545,3550,3541,3536,1199},
{3497,3554,3556,3564,3569,3573,3575,3581,3578,3583,3583,3537,3537,3534,3541,3544,3549,3545,3545,3547,3541,3537,1205},
{3476,3538,3541,3548,3550,3552,3556,3562,3560,3563,3560,3534,3536,3534,3539,3539,3547,3545,3547,3550,3546,3576,3787},
{3486,3541,3543,3550,3551,3552,3554,3558,3558,3560,3555,3540,3543,3542,3545,3546,3552,3550,3552,3552,3545,3529,1184},
{3489,3544,3544,3554,3551,3552,3556,3560,3558,3558,3553,3547,3552,3549,3550,3550,3556,3552,3554,3555,3547,3530,1174},
{3492,3550,3549,3558,3556,3557,3561,3568,3563,3562,3556,3557,3562,3560,3563,3561,3563,3562,3565,3568,3560,3545,1165},
{3484,3544,3544,3552,3551,3552,3556,3562,3556,3556,3550,3558,3562,3560,3564,3555,3558,3562,3569,3578,3573,3594,3831},
{3476,3536,3538,3546,3544,3547,3549,3550,3547,3549,3545,3558,3558,3555,3560,3558,3565,3566,3571,3575,3567,3554,1144},
{3471,3531,3535,3539,3537,3540,3541,3544,3541,3543,3540,3562,3560,3554,3558,3559,3568,3567,3570,3569,3560,3550,1136},
{3476,3535,3536,3539,3536,3540,3542,3544,3539,3541,3536,3578,3575,3567,3571,3568,3575,3573,3578,3575,3563,3552,1129},
{3474,3531,3530,3534,3533,3537,3541,3545,3539,3537,3529,3578,3576,3568,3575,3568,3576,3575,3583,3583,3576,3605,3841},
{3472,3524,3523,3531,3530,3532,3534,3538,3532,3534,3526,3591,3586,3581,3586,3584,3593,3589,3598,3602,3594,3586,1117},
{3396,3439,3440,3449,3447,3450,3451,3456,3451,3452,3447,4017,3906,3898,3890,3898,3914,3885,3907,3924,3924,3922,1111},
};

static short LowerLimit[13][23] = {	
{1940,1985,2000,2009,2024,2037,2028,2044,2053,2065,2085,1903,1905,1904,1908,1909,1911,1908,1909,1911,1906,1904,1199},
{1883,1913,1915,1919,1922,1923,1925,1928,1927,1929,1929,1905,1904,1903,1906,1908,1911,1908,1908,1910,1906,1904,1205},
{1871,1905,1906,1910,1912,1912,1914,1918,1916,1918,1917,1902,1904,1902,1905,1905,1909,1909,1909,1911,1909,1926,2039},
{1877,1907,1908,1911,1912,1912,1914,1916,1915,1916,1914,1906,1908,1907,1909,1909,1913,1912,1912,1912,1908,1900,1184},
{1879,1908,1909,1913,1912,1912,1914,1917,1915,1916,1913,1910,1912,1911,1912,1912,1915,1912,1914,1914,1910,1901,1174},
{1881,1912,1911,1916,1914,1915,1917,1921,1919,1918,1915,1915,1918,1916,1918,1917,1919,1918,1920,1921,1917,1909,1165},
{1876,1908,1909,1913,1912,1912,1914,1918,1915,1915,1912,1915,1918,1917,1919,1914,1915,1918,1922,1926,1923,1935,2063},
{1872,1904,1905,1910,1908,1910,1911,1912,1909,1911,1908,1916,1916,1914,1916,1916,1920,1920,1923,1925,1921,1914,1144},
{1869,1902,1904,1906,1904,1906,1907,1908,1906,1908,1906,1918,1916,1913,1916,1916,1921,1920,1922,1922,1917,1912,1136},
{1871,1903,1904,1906,1904,1905,1907,1908,1905,1907,1904,1926,1925,1920,1923,1921,1925,1923,1926,1925,1918,1912,1129},
{1871,1902,1900,1903,1902,1905,1906,1909,1905,1905,1900,1926,1925,1921,1925,1921,1925,1925,1929,1929,1925,1941,2072},
{1870,1897,1897,1901,1900,1902,1902,1905,1902,1902,1898,1933,1931,1928,1931,1930,1934,1933,1937,1939,1935,1931,1117},
{1829,1852,1853,1857,1856,1857,1858,1860,1858,1859,1856,2163,2103,2098,2094,2098,2107,2091,2104,2113,2113,2112,1111},
};




static struct rim4_f54_data *f54_data = NULL;
static short ImageArray[CFG_F54_TXCOUNT][CFG_F54_RXCOUNT];
static short HighResistance[HIGH_RESISTANCE_DATA_SIZE/2];
static void free_control_mem(void)
{
	struct f54_control control = f54_data->control;

	kfree(control.reg_0);
	kfree(control.reg_1);
	kfree(control.reg_2);
	kfree(control.reg_3);
	kfree(control.reg_4__6);
	kfree(control.reg_7);
	kfree(control.reg_8__9);
	kfree(control.reg_10);
	kfree(control.reg_11);
	kfree(control.reg_12__13);
	kfree(control.reg_14);
	kfree(control.reg_15);
	kfree(control.reg_16);
	kfree(control.reg_17);
	kfree(control.reg_18);
	kfree(control.reg_19);
	kfree(control.reg_20);
	kfree(control.reg_21);
	kfree(control.reg_22__26);
	kfree(control.reg_27);
	kfree(control.reg_28);
	kfree(control.reg_29);
	kfree(control.reg_30);
	kfree(control.reg_31);
	kfree(control.reg_32__35);
	kfree(control.reg_36);
	kfree(control.reg_37);
	kfree(control.reg_38);
	kfree(control.reg_39);
	kfree(control.reg_40);
	kfree(control.reg_41);
	kfree(control.reg_57);

	return;
}

static int synaptics_rmi4_f54_set_ctrl(void)
{
	unsigned char length;
	unsigned char reg_num = 0;
	unsigned char num_of_sensing_freqs;
	unsigned short reg_addr = f54_data->control_base_addr;
	struct f54_control *control = &f54_data->control;
	//struct synaptics_rmi4_data *rmi4_data = f54_data->rmi4_data;

	num_of_sensing_freqs = f54_data->query.number_of_sensing_frequencies;

	/* control 0 */
	//attrs_ctrl_regs_exist[reg_num] = true;
	control->reg_0 = kzalloc(sizeof(*(control->reg_0)),
			GFP_KERNEL);
	if (!control->reg_0)
		goto exit_no_mem;
	control->reg_0->address = reg_addr;
	reg_addr += sizeof(control->reg_0->data);
	reg_num++;

	/* control 1 */
	if ((f54_data->query.touch_controller_family == 0) ||
			(f54_data->query.touch_controller_family == 1)) {
		//attrs_ctrl_regs_exist[reg_num] = true;
		control->reg_1 = kzalloc(sizeof(*(control->reg_1)),
				GFP_KERNEL);
		if (!control->reg_1)
			goto exit_no_mem;
		control->reg_1->address = reg_addr;
		reg_addr += sizeof(control->reg_1->data);
	}
	reg_num++;

	/* control 2 */
	//attrs_ctrl_regs_exist[reg_num] = true;
	control->reg_2 = kzalloc(sizeof(*(control->reg_2)),
			GFP_KERNEL);
	if (!control->reg_2)
		goto exit_no_mem;
	control->reg_2->address = reg_addr;
	reg_addr += sizeof(control->reg_2->data);
	reg_num++;

	/* control 3 */
	if (f54_data->query.has_pixel_touch_threshold_adjustment == 1) {
		//attrs_ctrl_regs_exist[reg_num] = true;
		control->reg_3 = kzalloc(sizeof(*(control->reg_3)),
				GFP_KERNEL);
		if (!control->reg_3)
			goto exit_no_mem;
		control->reg_3->address = reg_addr;
		reg_addr += sizeof(control->reg_3->data);
	}
	reg_num++;

	/* controls 4 5 6 */
	if ((f54_data->query.touch_controller_family == 0) ||
			(f54_data->query.touch_controller_family == 1)) {
		//attrs_ctrl_regs_exist[reg_num] = true;
		control->reg_4__6 = kzalloc(sizeof(*(control->reg_4__6)),
				GFP_KERNEL);
		if (!control->reg_4__6)
			goto exit_no_mem;
		control->reg_4__6->address = reg_addr;
		reg_addr += sizeof(control->reg_4__6->data);
	}
	reg_num++;

	/* control 7 */
	if (f54_data->query.touch_controller_family == 1) {
		//attrs_ctrl_regs_exist[reg_num] = true;
		control->reg_7 = kzalloc(sizeof(*(control->reg_7)),
				GFP_KERNEL);
		if (!control->reg_7)
			goto exit_no_mem;
		control->reg_7->address = reg_addr;
		reg_addr += sizeof(control->reg_7->data);
	}
	reg_num++;

	/* controls 8 9 */
	if ((f54_data->query.touch_controller_family == 0) ||
			(f54_data->query.touch_controller_family == 1)) {
		//attrs_ctrl_regs_exist[reg_num] = true;
		control->reg_8__9 = kzalloc(sizeof(*(control->reg_8__9)),
				GFP_KERNEL);
		if (!control->reg_8__9)
			goto exit_no_mem;
		control->reg_8__9->address = reg_addr;
		reg_addr += sizeof(control->reg_8__9->data);
	}
	reg_num++;

	/* control 10 */
	if (f54_data->query.has_interference_metric == 1) {
		//attrs_ctrl_regs_exist[reg_num] = true;
		control->reg_10 = kzalloc(sizeof(*(control->reg_10)),
				GFP_KERNEL);
		if (!control->reg_10)
			goto exit_no_mem;
		control->reg_10->address = reg_addr;
		reg_addr += sizeof(control->reg_10->data);
	}
	reg_num++;

	/* control 11 */
	if (f54_data->query.has_ctrl11 == 1) {
		//attrs_ctrl_regs_exist[reg_num] = true;
		control->reg_11 = kzalloc(sizeof(*(control->reg_11)),
				GFP_KERNEL);
		if (!control->reg_11)
			goto exit_no_mem;
		control->reg_11->address = reg_addr;
		reg_addr += sizeof(control->reg_11->data);
	}
	reg_num++;

	/* controls 12 13 */
	if (f54_data->query.has_relaxation_control == 1) {
		//attrs_ctrl_regs_exist[reg_num] = true;
		control->reg_12__13 = kzalloc(sizeof(*(control->reg_12__13)),
				GFP_KERNEL);
		if (!control->reg_12__13)
			goto exit_no_mem;
		control->reg_12__13->address = reg_addr;
		reg_addr += sizeof(control->reg_12__13->data);
	}
	reg_num++;

	/* controls 14 15 16 */
	if (f54_data->query.has_sensor_assignment == 1) {
		//attrs_ctrl_regs_exist[reg_num] = true;

		control->reg_14 = kzalloc(sizeof(*(control->reg_14)),
				GFP_KERNEL);
		if (!control->reg_14)
			goto exit_no_mem;
		control->reg_14->address = reg_addr;
		reg_addr += sizeof(control->reg_14->data);

		control->reg_15 = kzalloc(sizeof(*(control->reg_15)),
				GFP_KERNEL);
		if (!control->reg_15)
			goto exit_no_mem;
		control->reg_15->length = f54_data->query.num_of_rx_electrodes;
		control->reg_15->data = kzalloc(control->reg_15->length *
				sizeof(*(control->reg_15->data)), GFP_KERNEL);
		if (!control->reg_15->data)
			goto exit_no_mem;
		control->reg_15->address = reg_addr;
		reg_addr += control->reg_15->length;

		control->reg_16 = kzalloc(sizeof(*(control->reg_16)),
				GFP_KERNEL);
		if (!control->reg_16)
			goto exit_no_mem;
		control->reg_16->length = f54_data->query.num_of_tx_electrodes;
		control->reg_16->data = kzalloc(control->reg_16->length *
				sizeof(*(control->reg_16->data)), GFP_KERNEL);
		if (!control->reg_16->data)
			goto exit_no_mem;
		control->reg_16->address = reg_addr;
		reg_addr += control->reg_16->length;
	}
	reg_num++;

	/* controls 17 18 19 */
	if (f54_data->query.has_sense_frequency_control == 1) {
		//attrs_ctrl_regs_exist[reg_num] = true;

		length = num_of_sensing_freqs;

		control->reg_17 = kzalloc(sizeof(*(control->reg_17)),
				GFP_KERNEL);
		if (!control->reg_17)
			goto exit_no_mem;
		control->reg_17->length = length;
		control->reg_17->data = kzalloc(length *
				sizeof(*(control->reg_17->data)), GFP_KERNEL);
		if (!control->reg_17->data)
			goto exit_no_mem;
		control->reg_17->address = reg_addr;
		reg_addr += length;

		control->reg_18 = kzalloc(sizeof(*(control->reg_18)),
				GFP_KERNEL);
		if (!control->reg_18)
			goto exit_no_mem;
		control->reg_18->length = length;
		control->reg_18->data = kzalloc(length *
				sizeof(*(control->reg_18->data)), GFP_KERNEL);
		if (!control->reg_18->data)
			goto exit_no_mem;
		control->reg_18->address = reg_addr;
		reg_addr += length;

		control->reg_19 = kzalloc(sizeof(*(control->reg_19)),
				GFP_KERNEL);
		if (!control->reg_19)
			goto exit_no_mem;
		control->reg_19->length = length;
		control->reg_19->data = kzalloc(length *
				sizeof(*(control->reg_19->data)), GFP_KERNEL);
		if (!control->reg_19->data)
			goto exit_no_mem;
		control->reg_19->address = reg_addr;
		reg_addr += length;
	}
	reg_num++;

	/* control 20 */
	//attrs_ctrl_regs_exist[reg_num] = true;
	control->reg_20 = kzalloc(sizeof(*(control->reg_20)),
			GFP_KERNEL);
	if (!control->reg_20)
		goto exit_no_mem;
	control->reg_20->address = reg_addr;
	reg_addr += sizeof(control->reg_20->data);
	reg_num++;

	/* control 21 */
	if (f54_data->query.has_sense_frequency_control == 1) {
		//attrs_ctrl_regs_exist[reg_num] = true;
		control->reg_21 = kzalloc(sizeof(*(control->reg_21)),
				GFP_KERNEL);
		if (!control->reg_21)
			goto exit_no_mem;
		control->reg_21->address = reg_addr;
		reg_addr += sizeof(control->reg_21->data);
	}
	reg_num++;

	/* controls 22 23 24 25 26 */
	if (f54_data->query.has_firmware_noise_mitigation == 1) {
		//attrs_ctrl_regs_exist[reg_num] = true;
		control->reg_22__26 = kzalloc(sizeof(*(control->reg_22__26)),
				GFP_KERNEL);
		if (!control->reg_22__26)
			goto exit_no_mem;
		control->reg_22__26->address = reg_addr;
		reg_addr += sizeof(control->reg_22__26->data);
	}
	reg_num++;

	/* control 27 */
	if (f54_data->query.has_iir_filter == 1) {
		//attrs_ctrl_regs_exist[reg_num] = true;
		control->reg_27 = kzalloc(sizeof(*(control->reg_27)),
				GFP_KERNEL);
		if (!control->reg_27)
			goto exit_no_mem;
		control->reg_27->address = reg_addr;
		reg_addr += sizeof(control->reg_27->data);
	}
	reg_num++;

	/* control 28 */
	if (f54_data->query.has_firmware_noise_mitigation == 1) {
		//attrs_ctrl_regs_exist[reg_num] = true;
		control->reg_28 = kzalloc(sizeof(*(control->reg_28)),
				GFP_KERNEL);
		if (!control->reg_28)
			goto exit_no_mem;
		control->reg_28->address = reg_addr;
		reg_addr += sizeof(control->reg_28->data);
	}
	reg_num++;

	/* control 29 */
	if (f54_data->query.has_cmn_removal == 1) {
		//attrs_ctrl_regs_exist[reg_num] = true;
		control->reg_29 = kzalloc(sizeof(*(control->reg_29)),
				GFP_KERNEL);
		if (!control->reg_29)
			goto exit_no_mem;
		control->reg_29->address = reg_addr;
		reg_addr += sizeof(control->reg_29->data);
	}
	reg_num++;

	/* control 30 */
	if (f54_data->query.has_cmn_maximum == 1) {
		//attrs_ctrl_regs_exist[reg_num] = true;
		control->reg_30 = kzalloc(sizeof(*(control->reg_30)),
				GFP_KERNEL);
		if (!control->reg_30)
			goto exit_no_mem;
		control->reg_30->address = reg_addr;
		reg_addr += sizeof(control->reg_30->data);
	}
	reg_num++;

	/* control 31 */
	if (f54_data->query.has_touch_hysteresis == 1) {
		//attrs_ctrl_regs_exist[reg_num] = true;
		control->reg_31 = kzalloc(sizeof(*(control->reg_31)),
				GFP_KERNEL);
		if (!control->reg_31)
			goto exit_no_mem;
		control->reg_31->address = reg_addr;
		reg_addr += sizeof(control->reg_31->data);
	}
	reg_num++;

	/* controls 32 33 34 35 */
	if (f54_data->query.has_edge_compensation == 1) {
		//attrs_ctrl_regs_exist[reg_num] = true;
		control->reg_32__35 = kzalloc(sizeof(*(control->reg_32__35)),
				GFP_KERNEL);
		if (!control->reg_32__35)
			goto exit_no_mem;
		control->reg_32__35->address = reg_addr;
		reg_addr += sizeof(control->reg_32__35->data);
	}
	reg_num++;

	/* control 36 */
	if ((f54_data->query.curve_compensation_mode == 1) ||
			(f54_data->query.curve_compensation_mode == 2)) {
		//attrs_ctrl_regs_exist[reg_num] = true;

		if (f54_data->query.curve_compensation_mode == 1) {
			length = max(f54_data->query.num_of_rx_electrodes,
					f54_data->query.num_of_tx_electrodes);
		} else if (f54_data->query.curve_compensation_mode == 2) {
			length = f54_data->query.num_of_rx_electrodes;
		}

		control->reg_36 = kzalloc(sizeof(*(control->reg_36)),
				GFP_KERNEL);
		if (!control->reg_36)
			goto exit_no_mem;
		control->reg_36->length = length;
		control->reg_36->data = kzalloc(length *
				sizeof(*(control->reg_36->data)), GFP_KERNEL);
		if (!control->reg_36->data)
			goto exit_no_mem;
		control->reg_36->address = reg_addr;
		reg_addr += length;
	}
	reg_num++;

	/* control 37 */
	if (f54_data->query.curve_compensation_mode == 2) {
		//attrs_ctrl_regs_exist[reg_num] = true;

		control->reg_37 = kzalloc(sizeof(*(control->reg_37)),
				GFP_KERNEL);
		if (!control->reg_37)
			goto exit_no_mem;
		control->reg_37->length = f54_data->query.num_of_tx_electrodes;
		control->reg_37->data = kzalloc(control->reg_37->length *
				sizeof(*(control->reg_37->data)), GFP_KERNEL);
		if (!control->reg_37->data)
			goto exit_no_mem;

		control->reg_37->address = reg_addr;
		reg_addr += control->reg_37->length;
	}
	reg_num++;

	/* controls 38 39 40 */
	if (f54_data->query.has_per_frequency_noise_control == 1) {
		//attrs_ctrl_regs_exist[reg_num] = true;

		control->reg_38 = kzalloc(sizeof(*(control->reg_38)),
				GFP_KERNEL);
		if (!control->reg_38)
			goto exit_no_mem;
		control->reg_38->length = num_of_sensing_freqs;
		control->reg_38->data = kzalloc(control->reg_38->length *
				sizeof(*(control->reg_38->data)), GFP_KERNEL);
		if (!control->reg_38->data)
			goto exit_no_mem;
		control->reg_38->address = reg_addr;
		reg_addr += control->reg_38->length;

		control->reg_39 = kzalloc(sizeof(*(control->reg_39)),
				GFP_KERNEL);
		if (!control->reg_39)
			goto exit_no_mem;
		control->reg_39->length = num_of_sensing_freqs;
		control->reg_39->data = kzalloc(control->reg_39->length *
				sizeof(*(control->reg_39->data)), GFP_KERNEL);
		if (!control->reg_39->data)
			goto exit_no_mem;
		control->reg_39->address = reg_addr;
		reg_addr += control->reg_39->length;

		control->reg_40 = kzalloc(sizeof(*(control->reg_40)),
				GFP_KERNEL);
		if (!control->reg_40)
			goto exit_no_mem;
		control->reg_40->length = num_of_sensing_freqs;
		control->reg_40->data = kzalloc(control->reg_40->length *
				sizeof(*(control->reg_40->data)), GFP_KERNEL);
		if (!control->reg_40->data)
			goto exit_no_mem;
		control->reg_40->address = reg_addr;
		reg_addr += control->reg_40->length;
	}
	reg_num++;

	/* control 41 */
	if (f54_data->query.has_signal_clarity == 1) {
		//attrs_ctrl_regs_exist[reg_num] = true;
		control->reg_41 = kzalloc(sizeof(*(control->reg_41)),
				GFP_KERNEL);
		if (!control->reg_41)
			goto exit_no_mem;
		control->reg_41->address = reg_addr;
		reg_addr += sizeof(control->reg_41->data);
	}
	reg_num++;

	/* control 42 */
	if (f54_data->query.has_variance_metric == 1)
		reg_addr += CONTROL_42_SIZE;

	/* controls 43 44 45 46 47 48 49 50 51 52 53 54 */
	if (f54_data->query.has_multi_metric_state_machine == 1)
		reg_addr += CONTROL_43_54_SIZE;

	/* controls 55 56 */
	if (f54_data->query.has_0d_relaxation_control == 1)
		reg_addr += CONTROL_55_56_SIZE;

	/* control 57 */
	if (f54_data->query.has_0d_acquisition_control == 1) {
		//attrs_ctrl_regs_exist[reg_num] = true;
		control->reg_57 = kzalloc(sizeof(*(control->reg_57)),
				GFP_KERNEL);
		if (!control->reg_57)
			goto exit_no_mem;
		control->reg_57->address = reg_addr;
		reg_addr += sizeof(control->reg_57->data);
	}
	reg_num++;

	/* control 58 */
	if (f54_data->query.has_0d_acquisition_control == 1)
		reg_addr += CONTROL_58_SIZE;

	/* control 59 */
	if (f54_data->query.has_h_blank == 1)
		reg_addr += CONTROL_59_SIZE;

	/* controls 60 61 62 */
	if ((f54_data->query.has_h_blank == 1) ||
			(f54_data->query.has_v_blank == 1) ||
			(f54_data->query.has_long_h_blank == 1))
		reg_addr += CONTROL_60_62_SIZE;

	/* control 63 */
	if ((f54_data->query.has_h_blank == 1) ||
			(f54_data->query.has_v_blank == 1) ||
			(f54_data->query.has_long_h_blank == 1) ||
			(f54_data->query.has_slew_metric == 1) ||
			(f54_data->query.has_slew_option == 1) ||
			(f54_data->query.has_noise_mitigation2 == 1))
		reg_addr += CONTROL_63_SIZE;

	/* controls 64 65 66 67 */
	if (f54_data->query.has_h_blank == 1)
		reg_addr += CONTROL_64_67_SIZE * 7;
	else if ((f54_data->query.has_v_blank == 1) ||
			(f54_data->query.has_long_h_blank == 1))
		reg_addr += CONTROL_64_67_SIZE;

	/* controls 68 69 70 71 72 73 */
	if ((f54_data->query.has_h_blank == 1) ||
			(f54_data->query.has_v_blank == 1) ||
			(f54_data->query.has_long_h_blank == 1))
		reg_addr += CONTROL_68_73_SIZE;

	/* control 74 */
	if (f54_data->query.has_slew_metric == 1)
		reg_addr += CONTROL_74_SIZE;

	/* control 75 */
	if (f54_data->query.has_enhanced_stretch == 1)
		reg_addr += num_of_sensing_freqs;

	/* control 76 */
	if (f54_data->query.has_startup_fast_relaxation == 1)
		reg_addr += CONTROL_76_SIZE;

	/* controls 77 78 */
	if (f54_data->query.has_esd_control == 1)
		reg_addr += CONTROL_77_78_SIZE;

	/* controls 79 80 81 82 83 */
	if (f54_data->query.has_noise_mitigation2 == 1)
		reg_addr += CONTROL_79_83_SIZE;

	/* controls 84 85 */
	if (f54_data->query.has_energy_ratio_relaxation == 1)
		reg_addr += CONTROL_84_85_SIZE;

	/* control 86 */
	if ((f54_data->query.has_query13 == 1) && (f54_data->query.has_ctrl86 == 1))
		reg_addr += CONTROL_86_SIZE;

	/* control 87 */
	if ((f54_data->query.has_query13 == 1) && (f54_data->query.has_ctrl87 == 1))
		reg_addr += CONTROL_87_SIZE;

	/* control 88 */
	if (f54_data->query.has_ctrl88 == 1) {
		control->reg_88 = kzalloc(sizeof(*(control->reg_88)),
				GFP_KERNEL);
		if (!control->reg_88)
			goto exit_no_mem;
		control->reg_88->address = reg_addr;
		reg_addr += sizeof(control->reg_88->data);
	}

	return 0;

exit_no_mem:
	printk("%s: Failed to alloc mem for control registers\n",__func__);
	return -ENOMEM;
}

static int do_preparation(void)
{
	int retval;
	unsigned char value;
	unsigned char command;
	unsigned char timeout_count;
	struct synaptics_rmi4_data *rmi4_data = f54_data->rmi4_data;

	mutex_lock(&f54_data->control_mutex);

	if (f54_data->query.touch_controller_family == 1) {
		value = 0;
		retval = rmi4_data->i2c_write(rmi4_data,
				f54_data->control.reg_7->address,
				&value,
				sizeof(f54_data->control.reg_7->data));
		if (retval < 0) {
			printk("%s: Failed to disable CBC\n",__func__);
			mutex_unlock(&f54_data->control_mutex);
			return retval;
		}
	} else if (f54_data->query.has_ctrl88 == 1) {
		retval = rmi4_data->i2c_read(rmi4_data,
				f54_data->control.reg_88->address,
				f54_data->control.reg_88->data,
				sizeof(f54_data->control.reg_88->data));
		if (retval < 0) {
			printk("%s: Failed to disable CBC (read ctrl88)\n",__func__);
			mutex_unlock(&f54_data->control_mutex);
			return retval;
		}
		f54_data->control.reg_88->cbc_polarity = 0;
		f54_data->control.reg_88->cbc_tx_carrier_selection = 0;
		retval = rmi4_data->i2c_write(rmi4_data,
				f54_data->control.reg_88->address,
				f54_data->control.reg_88->data,
				sizeof(f54_data->control.reg_88->data));
		if (retval < 0) {
			printk("%s: Failed to disable CBC (write ctrl88)\n",__func__);
			mutex_unlock(&f54_data->control_mutex);
			return retval;
		}
	}

	if (f54_data->query.has_0d_acquisition_control) {
		value = 0;
		retval = rmi4_data->i2c_write(rmi4_data,
				f54_data->control.reg_57->address,
				&value,
				sizeof(f54_data->control.reg_57->data));
		if (retval < 0) {
			printk("%s: Failed to disable 0D CBC\n",__func__);
			mutex_unlock(&f54_data->control_mutex);
			return retval;
		}
	}

	if (f54_data->query.has_signal_clarity) {
		value = 1;
		retval = rmi4_data->i2c_write(rmi4_data,
				f54_data->control.reg_41->address,
				&value,
				sizeof(f54_data->control.reg_41->data));
		if (retval < 0) {
			printk("%s: Failed to disable signal clarity\n",__func__);
			mutex_unlock(&f54_data->control_mutex);
			return retval;
		}
	}

	mutex_unlock(&f54_data->control_mutex);

	command = (unsigned char)COMMAND_FORCE_UPDATE;

	retval = rmi4_data->i2c_write(rmi4_data,
			f54_data->command_base_addr,
			&command,
			sizeof(command));
	if (retval < 0) {
		printk("%s: Failed to write force update command\n",__func__);
		return retval;
	}

	timeout_count = 0;
	do {
		retval = rmi4_data->i2c_read(rmi4_data,
				f54_data->command_base_addr,
				&value,
				sizeof(value));
		if (retval < 0) {
			printk("%s: Failed to read command register\n",__func__);
			return retval;
		}

		if (value == 0x00)
			break;

		msleep(100);
		timeout_count++;
	} while (timeout_count < FORCE_TIMEOUT_100MS);

	if (timeout_count == FORCE_TIMEOUT_100MS) {
		printk("%s: Timed out waiting for force update\n",__func__);
		return -ETIMEDOUT;
	}

	command = (unsigned char)COMMAND_FORCE_CAL;

	retval = rmi4_data->i2c_write(rmi4_data,
			f54_data->command_base_addr,
			&command,
			sizeof(command));
	if (retval < 0) {
		printk("%s: Failed to write force cal command\n",__func__);
		return retval;
	}

	timeout_count = 0;
	do {
		retval = rmi4_data->i2c_read(rmi4_data,
				f54_data->command_base_addr,
				&value,
				sizeof(value));
		if (retval < 0) {
			printk("%s: Failed to read command register\n",__func__);
			return retval;
		}

		if (value == 0x00)
			break;

		msleep(100);
		timeout_count++;
	} while (timeout_count < FORCE_TIMEOUT_100MS);

	if (timeout_count == FORCE_TIMEOUT_100MS) {
		printk("%s: Timed out waiting for force cal\n",__func__);
		return -ETIMEDOUT;
	}

	return 0;
}

static int synaptics_rmi4_f54_sw_reset(struct synaptics_rmi4_data *rmi4_data)
{
	int retval;
	unsigned char command = 0x01;

	retval = rmi4_data->i2c_write(rmi4_data,
			rmi4_data->f01_cmd_base_addr,
			&command,
			sizeof(command));
	if (retval < 0)
		return retval;

	msleep(100);

//	if (rmi4_data->hw_if->ui_hw_init) {
//		retval = rmi4_data->hw_if->ui_hw_init(rmi4_data);
//		if (retval < 0)
//			return retval;
//	}

	return 0;
}

static void set_report_size(void)
{
	//int retval;
	unsigned char rx = f54_data->rx_assigned;
	unsigned char tx = f54_data->tx_assigned;
	//struct synaptics_rmi4_data *rmi4_data = f54_data->rmi4_data;

	switch (f54_data->report_type) {
	case F54_8BIT_IMAGE:
		f54_data->report_size = rx * tx;
		break;
	case F54_16BIT_IMAGE:
	case F54_RAW_16BIT_IMAGE:
	case F54_TRUE_BASELINE:
	case F54_FULL_RAW_CAP:
	case F54_FULL_RAW_CAP_RX_COUPLING_COMP:
	case F54_SENSOR_SPEED:
		f54_data->report_size = 2 * rx * tx;
		break;
	case F54_HIGH_RESISTANCE:
		f54_data->report_size = HIGH_RESISTANCE_DATA_SIZE;
		break;
	case F54_TX_TO_TX_SHORT:
	case F54_TX_OPEN:
	case F54_TX_TO_GROUND:
		f54_data->report_size = (tx + 7) / 8;
		break;
	case F54_RX_TO_RX1:
	case F54_RX_OPENS1:
		if (rx < tx)
			f54_data->report_size = 2 * rx * rx;
		else
			f54_data->report_size = 2 * rx * tx;
		break;
	case F54_FULL_RAW_CAP_MIN_MAX:
		f54_data->report_size = FULL_RAW_CAP_MIN_MAX_DATA_SIZE;
		break;
	case F54_RX_TO_RX2:
	case F54_RX_OPENS2:
		if (rx <= tx)
			f54_data->report_size = 0;
		else
			f54_data->report_size = 2 * rx * (rx - tx);
		break;
	case F54_ADC_RANGE:
		//not support 
		f54_data->report_size = 2 * rx * tx;
		break;
	case F54_TREX_OPENS:
	case F54_TREX_TO_GND:
	case F54_TREX_SHORTS:
		f54_data->report_size = TREX_DATA_SIZE;
		break;
	default:
		f54_data->report_size = 0;
	}

	return;
}

static bool is_report_type_valid(enum f54_report_types report_type)
{
	switch (report_type) {
	case F54_8BIT_IMAGE:
	case F54_16BIT_IMAGE:
	case F54_RAW_16BIT_IMAGE:
	case F54_HIGH_RESISTANCE:
	case F54_TX_TO_TX_SHORT:
	case F54_RX_TO_RX1:
	case F54_TRUE_BASELINE:
	case F54_FULL_RAW_CAP_MIN_MAX:
	case F54_RX_OPENS1:
	case F54_TX_OPEN:
	case F54_TX_TO_GROUND:
	case F54_RX_TO_RX2:
	case F54_RX_OPENS2:
	case F54_FULL_RAW_CAP:
	case F54_FULL_RAW_CAP_RX_COUPLING_COMP:
	case F54_SENSOR_SPEED:
	case F54_ADC_RANGE:
	case F54_TREX_OPENS:
	case F54_TREX_TO_GND:
	case F54_TREX_SHORTS:
		return true;
		break;
	default:
		f54_data->report_type = INVALID_REPORT_TYPE;
		f54_data->report_size = 0;
		return false;
	}
}

static int synaptics_f54_set_report_type(unsigned long val)
{
	enum f54_report_types type;
	type = (enum f54_report_types)val;
	if(f54_data == NULL)
		return -1;
	if(is_report_type_valid(type)) {
		f54_data->report_type = type;
	}else
		return -1;
	printk("%s,tpd report_type = %d \n",__func__, f54_data->report_type);
	return 0;
}

//EXPORT_SYMBOL(synaptics_f54_set_report_type);

static int synaptics_f54_get_report_type(void)
{
	if(f54_data == NULL)
		return -1;
	return f54_data->report_type;
}
//EXPORT_SYMBOL(synaptics_f54_get_report_type);
static int synaptics_rmi4_f54_check_full_raw_data(void)
{
	int i,j;
	//int max=0;
	//int min=65535;
	
	for (i = 0; i < f54_data->tx_assigned; i++) {   
	   for (j = 0; j <( f54_data->rx_assigned -1); j++)
	   {
		    if(ImageArray[i][j] < LowerLimit[i][j] || ImageArray[i][j] > UpperLimit[i][j]) {
				printk("%s, tpd CTP ITO maybe broken. AmageArray[%d][%d]=%d\n",__func__,i,j,ImageArray[i][j]);
				return 0;

			}
	   }
   	}

        for (i = 2; i < f54_data->tx_assigned ; i += 4) {
		    if(ImageArray[i][ f54_data->rx_assigned-1] < LowerLimit[i][f54_data->rx_assigned-1] || ImageArray[i][f54_data->rx_assigned-1] > UpperLimit[i][f54_data->rx_assigned-1]) {
				printk("%s, tpd CTP ITO maybe broken. AmageArray[%d][%d]=%d\n",__func__,i,j,ImageArray[i][j]);
				return 0;

		    }
	}
			
	return 1;
}

static int synaptics_rmi4_f54_check_high_resistance_data(void)
{
	if(HighResistance[0] > F54_HR_RX_OFFSET)
		return 0;
	if(HighResistance[1] > F54_HR_TX_OFFSET)
		return 0;
	if(HighResistance[2] < F54_HR_MIN)
		return 0;
	return 1;
}
int synaptics_rmi4_f54_get_report_data(void)
{
	int retval;
	unsigned char report_index[2];
	unsigned char command;
	//unsigned short length;
	unsigned int patience = 250;
	int i, j, k =0;
	//short *data;
	struct synaptics_rmi4_data *rmi4_data = f54_data->rmi4_data;
	
	if(f54_data == NULL){
		printk("%s: tpd f54_data is null \n",__func__);
		return EINVAL;
	}
	
	
	 // Set report mode to to run the AutoScan
	if(!is_report_type_valid(f54_data->report_type)) {
		printk("%s: tpd report_type is invalid \n",__func__);
		retval = -EINVAL;
		goto error_exit;
	}
	command = f54_data->report_type;
	retval = rmi4_data->i2c_write(rmi4_data,
	 		f54_data->data_base_addr,
	 		&command, sizeof(command));
	if (retval < 0) {
		printk("%s: tpd Failed to write command=0x%x\n",__func__, command);
		retval = -EINVAL;
		goto error_exit;
	}

	// Set the GetReport bit to run the AutoScan
	command = 0x01;
	retval = rmi4_data->i2c_write(rmi4_data,
			f54_data->command_base_addr,
			&command, sizeof(command));
	if (retval < 0) {
		printk("%s: tpd Failed to write command 0x01\n",__func__);
		retval = -EINVAL;
		goto error_exit;
	}
	do {	
		retval = rmi4_data->i2c_read(rmi4_data,
			f54_data->command_base_addr, &command, sizeof(command));

		if(retval < 0) {
			printk("%s: tpd Failed to read command 0x01 \n",__func__);
			retval = -EINVAL;
			goto error_exit;
		} 
		if(command & COMMAND_GET_REPORT)
			msleep(20);
		else
			break;
	} while (--patience > 0);
	//mdelay(20);
	if(command & COMMAND_GET_REPORT) {
		printk("%s: tpd Time out waiting for report ready \n",__func__);
		retval = -EINVAL;
		goto error_exit;
	}

	set_report_size();
	if (f54_data->report_size == 0) {
		printk("%s: tpd Report data size = 0\n",__func__);
		retval = -EINVAL;
		goto error_exit;
	}

	if (f54_data->data_buffer_size < f54_data->report_size) {
		mutex_lock(&f54_data->data_mutex);
		if (f54_data->data_buffer_size)
			kfree(f54_data->report_data);
		f54_data->report_data = kzalloc(f54_data->report_size, GFP_KERNEL);
		if (!f54_data->report_data) {
			printk("%s: Failed to alloc mem for data buffer\n",__func__);
			f54_data->data_buffer_size = 0;
			mutex_unlock(&f54_data->data_mutex);
			retval = -ENOMEM;
			goto error_exit;
		}
		f54_data->data_buffer_size = f54_data->report_size;
		mutex_unlock(&f54_data->data_mutex);
	}

	report_index[0] = 0;
	report_index[1] = 0;
	retval = rmi4_data->i2c_write(rmi4_data,
			f54_data->data_base_addr + DATA_REPORT_INDEX_OFFSET,
			report_index,
			sizeof(report_index));
	if (retval < 0) {
		printk("%s: tpd Failed to write index\n",__func__);
		retval = -EINVAL;
		goto error_exit;
	}
	
	
	//to read data
	retval = rmi4_data->i2c_read(rmi4_data,
			f54_data->data_base_addr + DATA_REPORT_DATA_OFFSET,
			f54_data->report_data,f54_data->report_size);
	if (retval < 0) {
		printk("%s: tpd Failed to read image data\n",__func__);
		retval = -EINVAL;
		goto error_exit;
	}

	if(f54_data->report_type == F54_HIGH_RESISTANCE) {
		printk("tpd [");
		for(i = 0; i < HIGH_RESISTANCE_DATA_SIZE; i+=2) {
			HighResistance[i/2] = (short) (f54_data->report_data[i]|(f54_data->report_data[i+1] << 8));
			printk("%d ,",HighResistance[i/2]);
		}
		printk("]\n");
		return 0;
	}
	
	for (i = 0; i < f54_data->tx_assigned; i++)
	{
		for (j = 0; j < f54_data->rx_assigned; j++)
		{
			ImageArray[i][j] =(short) (f54_data->report_data[k]|(f54_data->report_data[k+1] << 8));
			k = k + 2;
		}  	  
	}

	printk("%s: tpd query_base_addr:0x%04x; data_base_addr:0x%04x; tx:%d; rx:%d \n",__func__,f54_data->query_base_addr,
			f54_data->data_base_addr,f54_data->tx_assigned,f54_data->rx_assigned);
   // Check against test limits
   		printk("\n tpd The test result:\n");
   	if(f54_data->report_type == F54_16BIT_IMAGE) {
		for (j = 0; j < f54_data->rx_assigned; j++)
		{   
			printk("tpd [ ");
			for (i = 0; i < f54_data->tx_assigned; i++) {
				printk("(%d),",ImageArray[i][j]);
			}
			printk("]\n");
		}
   	}else {
		for (i = 0; i < f54_data->tx_assigned; i++)
		{   
		   printk("tpd [ ");
		   for (j = 0; j < f54_data->rx_assigned; j++) {
				printk("(%d),",ImageArray[i][j]);
		   }
		   printk("]\n");
		}
	}
   
	return 0;
error_exit:

	return retval;
}


int synaptics_rmi4_f54_command_func(int command, int value)
{
	int retval = 0;
	switch (command) {
		case F54_SELF_TEST:
			synaptics_f54_set_report_type(F54_FULL_RAW_CAP_RX_COUPLING_COMP);
			retval = do_preparation();
			if (retval < 0) {
				printk("%s: tpd Failed to do preparation\n",__func__);
				return retval;
			}
			retval = synaptics_rmi4_f54_get_report_data();
			if(retval < 0) {
				synaptics_rmi4_f54_sw_reset(f54_data->rmi4_data);
				return retval;
			}
			
			synaptics_f54_set_report_type(F54_HIGH_RESISTANCE);
			retval = synaptics_rmi4_f54_get_report_data();
			if(retval < 0) {
				synaptics_rmi4_f54_sw_reset(f54_data->rmi4_data);
				return retval;
			}
			
			synaptics_rmi4_f54_sw_reset(f54_data->rmi4_data);
			
			retval = (synaptics_rmi4_f54_check_full_raw_data() & synaptics_rmi4_f54_check_high_resistance_data());
		
			break;
		case F54_GET_REPORT_TYPE:
			retval = synaptics_f54_get_report_type();
			break;
		case F54_SET_REPORT_TYPE:
			retval = synaptics_f54_set_report_type(value);
			break;
		case F54_GET_REPORT_DATA:
			retval = synaptics_rmi4_f54_get_report_data();
			break;
		default:
			retval = -1;
			break;
	}
	return retval;
}
EXPORT_SYMBOL(synaptics_rmi4_f54_command_func);



static void synaptics_rmi4_f54_data_set_regs(struct synaptics_rmi4_data *rmi4_data,
		struct synaptics_rmi4_fn_desc *fd,	unsigned char page)
{
	f54_data->query_base_addr = fd->query_base_addr | (page << 8);
	f54_data->control_base_addr = fd->ctrl_base_addr | (page << 8);
	f54_data->data_base_addr = fd->data_base_addr | (page << 8);
	f54_data->command_base_addr = fd->cmd_base_addr | (page << 8);
	return;
}
int synaptics_rmi4_f54_data_init(struct synaptics_rmi4_data *rmi4_data)
{
	int retval;
	unsigned short pdt_entry_addr;
	unsigned char page_number;
	//unsigned char rx_tx[2];

	bool f54found = false;
	
	struct synaptics_rmi4_fn_desc rmi_fd;
	if(f54_data == NULL) {
		f54_data = kzalloc(sizeof(*f54_data), GFP_KERNEL);
		if (!f54_data) {
			printk("%s: Failed to alloc mem for f54\n",__func__);
			retval = -ENOMEM;
			goto exit;
		}
	}
	f54_data->rmi4_data = rmi4_data;
	/* Scan the page description tables of the pages to service */
	for (page_number = 0; page_number < PAGES_TO_SERVICE; page_number++) {
		for (pdt_entry_addr = PDT_START; pdt_entry_addr > PDT_END;
				pdt_entry_addr -= PDT_ENTRY_SIZE) {
			pdt_entry_addr |= (page_number << 8);

			retval = rmi4_data->i2c_read(rmi4_data,
					pdt_entry_addr,
					(unsigned char *)&rmi_fd,
					sizeof(rmi_fd));
			if (retval < 0)
				return retval;

			pdt_entry_addr &= ~(MASK_8BIT << 8);

			if (rmi_fd.fn_number == 0) {
				printk("%s: Reached end of PDT\n",__func__);
				break;
			}

			printk("%s: F%02x found (page %d)\n",__func__, rmi_fd.fn_number,
					page_number);

			switch (rmi_fd.fn_number) {
			case SYNAPTICS_RMI4_F54:
				synaptics_rmi4_f54_data_set_regs(rmi4_data,
						&rmi_fd, page_number);
				f54found = true;
				break;
			default:
				break;
			}

			if (f54found)
				goto pdt_done;

		}
	}

	if (!f54found) {
		retval = -ENODEV;
		goto exit_free_f54;
	}

pdt_done:
	retval = rmi4_data->i2c_read(rmi4_data,
			f54_data->query_base_addr,
			f54_data->query.data,
			sizeof(f54_data->query.data));
	if (retval < 0) {
		printk("%s: Failed to read f54 query registers\n",__func__);
		goto exit;
	}

	retval = synaptics_rmi4_f54_set_ctrl();
	if (retval < 0) {
		printk("%s: Failed to set up f54 control registers\n",__func__);
		goto exit_free_control;
	}
	
	mutex_init(&f54_data->data_mutex);
	mutex_init(&f54_data->control_mutex);

	f54_data->rx_assigned =CFG_F54_RXCOUNT ;// rx_tx[0];
	f54_data->tx_assigned =CFG_F54_TXCOUNT ;//rx_tx[1];
	f54_data->report_size = f54_data->rx_assigned * f54_data->tx_assigned *2;
	f54_data->report_type = 0;
	return 0;

exit_free_control:
	free_control_mem();	
exit_free_f54:
	kfree(f54_data);
	f54_data = NULL;

exit:
	return retval;
}
EXPORT_SYMBOL(synaptics_rmi4_f54_data_init);

MODULE_AUTHOR("Synaptics, Inc.");
MODULE_DESCRIPTION("Synaptics DSX Test Reporting Module");
MODULE_LICENSE("GPL v2");

